﻿-- phpMyAdmin SQL Dump
-- version 2.11.3
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Mai 29, 2013 as 11:55 AM
-- Versão do Servidor: 5.0.51
-- Versão do PHP: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Banco de Dados: `painel_master`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigos`
--

CREATE TABLE `artigos` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `autor` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `artigos`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `campanha_newsletter`
--

CREATE TABLE `campanha_newsletter` (
  `id` int(11) NOT NULL auto_increment,
  `data` varchar(100) collate utf8_unicode_ci NOT NULL,
  `topico1` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo1` text collate utf8_unicode_ci NOT NULL,
  `texto1` text collate utf8_unicode_ci NOT NULL,
  `link1` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem1` text collate utf8_unicode_ci NOT NULL,
  `topico2` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo2` text collate utf8_unicode_ci NOT NULL,
  `texto2` text collate utf8_unicode_ci NOT NULL,
  `link2` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem2` text collate utf8_unicode_ci NOT NULL,
  `topico3` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo3` text collate utf8_unicode_ci NOT NULL,
  `texto3` text collate utf8_unicode_ci NOT NULL,
  `link3` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem3` text collate utf8_unicode_ci NOT NULL,
  `topico4` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo4` text collate utf8_unicode_ci NOT NULL,
  `texto4` text collate utf8_unicode_ci NOT NULL,
  `link4` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem4` text collate utf8_unicode_ci NOT NULL,
  `topico5` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo5` text collate utf8_unicode_ci NOT NULL,
  `texto5` text collate utf8_unicode_ci NOT NULL,
  `link5` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem5` text collate utf8_unicode_ci NOT NULL,
  `topico6` varchar(255) collate utf8_unicode_ci NOT NULL,
  `titulo6` text collate utf8_unicode_ci NOT NULL,
  `texto6` text collate utf8_unicode_ci NOT NULL,
  `link6` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem6` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `campanha_newsletter`
--

INSERT INTO `campanha_newsletter` (`id`, `data`, `topico1`, `titulo1`, `texto1`, `link1`, `imagem1`, `topico2`, `titulo2`, `texto2`, `link2`, `imagem2`, `topico3`, `titulo3`, `texto3`, `link3`, `imagem3`, `topico4`, `titulo4`, `texto4`, `link4`, `imagem4`, `topico5`, `titulo5`, `texto5`, `link5`, `imagem5`, `topico6`, `titulo6`, `texto6`, `link6`, `imagem6`) VALUES
(14, '2012-03-30', 'Destaque', 'Domingo do Advento', '<p style="text-align: justify;"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type a<strong>nd scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially un</strong>changed. <span style="color: #ff9900;"><strong>It was </strong></span><em><span style="color: #ff9900;"><strong>popularised i</strong></span>n the 1960s with the release</em> of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'www.santuarioperpetuosocorro.org.br/', 'fotos/3640136728032012.jpg', 'Ãšltimas NotÃ­cias', 'Dia de SÃ£o SebastiÃ£o', '<p>It is a long established fact that a <strong>reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal</strong> distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'www.santuarioperpetuosocorro.org.br/', 'fotos/55236928032012.jpg', 'VÃ­deo', 'Fique Ligado', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', 'www.santuarioperpetuosocorro.org.br/videos_fique_ligado.php', 'fotos/7360229528032012.jpg', 'Pelo Brasil e Pelo Mundo', 'Dia da Campanha', '<p><span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here. Teste de acentua&ccedil;&atilde;o.</span></p>', 'www.santuarioperpetuosocorro.org.br/novidades.php', 'fotos/5498962429032012.jpg', 'Homilias', 'Palavra do Reitor', '<p><span>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</span></p>', 'www.santuarioperpetuosocorro.org.br/novidades.php', 'fotos/6435546929032012.jpg', 'Galeria de VÃ­deos', 'Festa da Padroeira', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing.</span></p>', 'www.santuarioperpetuosocorro.org.br/novidades.php', 'fotos/3807067928032012.jpg'),
(18, '2012-04-03', 'TÃ³pico 1', 'The standard Lorem', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', 'google.com', 'fotos/6929626403042012.jpg', '', '', '', 'santuarioperpetuosocorro.org.br/', 'fotos/3423461903042012.jpg', 'TÃ³pico 3', 'TÃ­tulo 3', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', 'santuarioperpetuosocorro.org.br/videos_fique_ligado.php', 'fotos/6612243603042012.jpg', 'TÃ³pico 4', 'Tti 4', '<p><span style="font-family: arial, helvetica, sans-serif;">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.</span></p>', 'santuarioperpetuosocorro.org.br/novidades.php', 'fotos/7783203103042012.jpg', 'Top 5', 'titulo 5', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.&nbsp;</span></p>', 'santuarioperpetuosocorro.org.br/novidades.php', 'fotos/7136535603042012.jpg', '', '', '', 'santuarioperpetuosocorro.org.br/novidades.php', 'fotos/3993530303042012.jpg'),
(25, '2012-08-15', '', 'Folder', '', 'santuario.com.br', 'fotos/2500305215082012.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`, `data`, `hora`, `status`) VALUES
(1, 'CosmÃ©ticos', '2012-03-29', '00:00:00', 'publicado(a)'),
(2, 'Delineadores', '2012-03-30', '10:22:11', 'publicado(a)'),
(3, 'Frisadores', '0000-00-00', '00:00:00', 'publicado(a)'),
(4, 'Carbox', '0000-00-00', '00:00:00', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL auto_increment,
  `comentario` text collate utf8_unicode_ci NOT NULL,
  `id_post` int(11) NOT NULL,
  `categoria_post` varchar(255) collate utf8_unicode_ci NOT NULL,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `comentarios`
--

INSERT INTO `comentarios` (`id`, `comentario`, `id_post`, `categoria_post`, `nome`, `data`, `hora`, `status`) VALUES
(1, 'Teste de comentario de teste de publicacao do site da inlove.', 2, '', 'Maria Carolina', '2011-11-23', '00:00:00', 'publicado(a)'),
(3, 'Teremos um cantinho de beleza da Sephora quinta-feira, na loja de Ipanema.', 2, '', 'Felipe', '2011-11-24', '00:00:00', 'publicado(a)'),
(4, 'Animada pras festas de fim de ano? Preparamos uma seleÃ§Ã£o.', 2, '', 'Maria Joaquina', '2011-11-25', '00:00:00', 'publicado(a)'),
(5, '<p>Kristen abre o cora&ccedil;&atilde;o: fala sobre as cenas do casamento e muito mais!</p>', 2, '', 'Manuela', '2011-11-25', '10:05:24', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes_newsletter`
--

CREATE TABLE `configuracoes_newsletter` (
  `id` int(11) NOT NULL auto_increment,
  `cabecalho` text collate utf8_unicode_ci NOT NULL,
  `bgcolor` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descadastramento` text collate utf8_unicode_ci NOT NULL,
  `nome_remetente` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email_remetente` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `configuracoes_newsletter`
--

INSERT INTO `configuracoes_newsletter` (`id`, `cabecalho`, `bgcolor`, `descadastramento`, `nome_remetente`, `email_remetente`) VALUES
(1, 'SantuÃ¡rio Nossa Senhora do PerpÃ©tuo Socorro, Campos dos Goytacazes', '1D57CC', 'santuarioperpetuosocorro.org.br', 'Art NegÃ³cios - ImÃ³veis e AdministraÃ§Ã£o', 'contato@artnegocios.org.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `emails_newsletter`
--

CREATE TABLE `emails_newsletter` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(50) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status_char` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=893 ;

--
-- Extraindo dados da tabela `emails_newsletter`
--

INSERT INTO `emails_newsletter` (`id`, `nome`, `email`, `status_char`, `status`) VALUES
(1, '', 'felipe@globalnetsis.com.br', 'bloqueado(a)', 0),
(170, '', ' gilsonsilveira@petrobras.com.br', 'publicado(a)', 0),
(5, '', 'contato@globalnetsis.com.br', 'bloqueado(a)', 0),
(171, '', ' arthur.becker@petrobras.com.br', 'publicado(a)', 0),
(163, '', ' ademil@petrobras.com.br', 'publicado(a)', 0),
(164, '', ' miguel.justino@petrobras.com.br', 'publicado(a)', 0),
(165, '', ' leitinho@petrobras.com.br', 'publicado(a)', 0),
(166, '', ' antony.mendes@petrobras.com.br', 'publicado(a)', 0),
(167, '', ' felipperodriguez@petrobras.com.br', 'publicado(a)', 0),
(168, '', ' psjunior@petrobras.com.br', 'publicado(a)', 0),
(169, '', ' adrianapereira@petrobras.com.br', 'publicado(a)', 0),
(159, '', ' pfhaulo@petrobras.com.br', 'publicado(a)', 0),
(160, '', ' wolny@petrobras.com.br', 'publicado(a)', 0),
(161, '', ' jmessias@petrobras.com.br', 'publicado(a)', 0),
(162, '', ' claudiosoares@petrobras.com.br', 'publicado(a)', 0),
(155, '', ' satiro1@petrobras.com.br', 'publicado(a)', 0),
(156, '', ' rdasobrinho@petrobras.com.br', 'publicado(a)', 0),
(157, '', ' heluysio@petrobras.com.br', 'publicado(a)', 0),
(158, '', ' hnunes@petrobras.com.br', 'publicado(a)', 0),
(150, '', ' zenewton@petrobras.com.br', 'publicado(a)', 0),
(151, '', ' cido@petrobras.com.br', 'publicado(a)', 0),
(152, '', ' jonasreis@petrobras.com.br', 'publicado(a)', 0),
(153, '', ' aldirsouza@petrobras.com.br', 'publicado(a)', 0),
(154, '', ' luis.carlos@petrobras.com.br', 'publicado(a)', 0),
(71, '', ' luiz.alexandre@petrobras.com.br', 'publicado(a)', 0),
(72, '', ' vjal@petrobras.com.br', 'publicado(a)', 0),
(73, '', ' rgsantos@petrobras.com.br', 'publicado(a)', 0),
(74, '', ' sawsilva@petrobras.com.br', 'publicado(a)', 0),
(75, '', ' acpgomes@petrobras.com.br', 'publicado(a)', 0),
(76, '', ' rgalvao@petrobras.com.br', 'publicado(a)', 0),
(77, '', ' asoares@petrobras.com.br', 'publicado(a)', 0),
(78, '', ' jora@petrobras.com.br', 'publicado(a)', 0),
(79, '', ' rosalvogonzaga@petrobras.com.br', 'publicado(a)', 0),
(80, '', ' luizcruz@petrobras.com.br', 'publicado(a)', 0),
(81, '', ' noronha_frederico@petrobras.com.br', 'publicado(a)', 0),
(82, '', ' claudiofera@petrobras.com.br', 'publicado(a)', 0),
(83, '', ' semim@petrobras.com.br', 'publicado(a)', 0),
(84, '', ' alvessousa@petrobras.com.br', 'publicado(a)', 0),
(85, '', ' wiltonf@petrobras.com.br', 'publicado(a)', 0),
(86, '', ' arifalcao@petrobras.com.br', 'publicado(a)', 0),
(87, '', ' venilton@petrobras.com.br', 'publicado(a)', 0),
(88, '', ' odailson@petrobras.com.br', 'publicado(a)', 0),
(89, '', ' arobson@petrobras.com.br', 'publicado(a)', 0),
(90, '', ' paesato@petrobras.com.br', 'publicado(a)', 0),
(91, '', ' ianki@petrobras.com.br', 'publicado(a)', 0),
(92, '', ' rodgero@petrobras.com.br', 'publicado(a)', 0),
(93, '', ' mvsc@petrobras.com.br', 'publicado(a)', 0),
(94, '', ' amnogueira@petrobras.com.br', 'publicado(a)', 0),
(95, '', ' fujihara@petrobras.com.br', 'publicado(a)', 0),
(96, '', ' spena@petrobras.com.br', 'publicado(a)', 0),
(97, '', ' marcelopinho@petrobras.com.br', 'publicado(a)', 0),
(98, '', ' efm@petrobras.com.br', 'publicado(a)', 0),
(99, '', ' gambinni@petrobras.com.br', 'publicado(a)', 0),
(100, '', ' xandy@petrobras.com.br', 'publicado(a)', 0),
(101, '', ' anjelo@petrobras.com.br', 'publicado(a)', 0),
(102, '', ' adilton.silva@petrobras.com.br', 'publicado(a)', 0),
(103, '', ' betotavares@petrobras.com.br', 'publicado(a)', 0),
(104, '', ' costacortez@petrobras.com.br', 'publicado(a)', 0),
(105, '', ' gabrieleduardo@petrobras.com.br', 'publicado(a)', 0),
(106, '', ' solrac@petrobras.com.br', 'publicado(a)', 0),
(107, '', ' gsantana@petrobras.com.br', 'publicado(a)', 0),
(108, '', ' chicaogomes@petrobras.com.br', 'publicado(a)', 0),
(109, '', ' marcomartins@petrobras.com.br', 'publicado(a)', 0),
(110, '', ' marcostavares@petrobras.com.br', 'publicado(a)', 0),
(111, '', ' mvcrespo@petrobras.com.br', 'publicado(a)', 0),
(112, '', ' ariane@petrobras.com.br', 'publicado(a)', 0),
(113, '', ' cesarfm@petrobras.com.br', 'publicado(a)', 0),
(114, '', ' edinil@petrobras.com.br', 'publicado(a)', 0),
(115, '', ' jaas@petrobras.com.br', 'publicado(a)', 0),
(116, '', ' jabravo@petrobras.com.br', 'publicado(a)', 0),
(117, '', ' marcioaffonso@petrobras.com.br', 'publicado(a)', 0),
(118, '', ' edinaldo@petrobras.com.br', 'publicado(a)', 0),
(119, '', ' jsamat@petrobras.com.br', 'publicado(a)', 0),
(120, '', ' jtmjjr@petrobras.com.br', 'publicado(a)', 0),
(121, '', ' tiagorocha@petrobras.com.br', 'publicado(a)', 0),
(122, '', ' noell@petrobras.com.br', 'publicado(a)', 0),
(123, '', ' rikardo@petrobras.com.br', 'publicado(a)', 0),
(124, '', ' crgsilva@petrobras.com.br', 'publicado(a)', 0),
(125, '', ' julioleite@petrobras.com.br', 'publicado(a)', 0),
(126, '', ' edrock@petrobras.com.br', 'publicado(a)', 0),
(127, '', ' jorgeandref@petrobras.com.br', 'publicado(a)', 0),
(128, '', ' rafasimon@petrobras.com.br', 'publicado(a)', 0),
(129, '', ' nelson@petrobras.com.br', 'publicado(a)', 0),
(130, '', ' mario_sv@petrobras.com.br', 'publicado(a)', 0),
(131, '', ' cpgoncalves@petrobras.com.br', 'publicado(a)', 0),
(132, '', ' michelfranco@petrobras.com.br', 'publicado(a)', 0),
(133, '', ' jorge.galvao@petrobras.com.br', 'publicado(a)', 0),
(134, '', ' paulovieira@petrobras.com.br', 'publicado(a)', 0),
(135, '', ' carlosfarias@petrobras.com.br', 'publicado(a)', 0),
(136, '', ' hesteves@petrobras.com.br', 'publicado(a)', 0),
(137, '', ' piaba@petrobras.com.br', 'publicado(a)', 0),
(138, '', ' chicolopes@petrobras.com.br', 'publicado(a)', 0),
(139, '', ' v.nunes@petrobras.com.br', 'publicado(a)', 0),
(140, '', ' menezesrobson@petrobras.com.br', 'publicado(a)', 0),
(141, '', ' adilsonprazeres@petrobras.com.br', 'publicado(a)', 0),
(142, '', ' dourival@petrobras.com.br', 'publicado(a)', 0),
(143, '', ' egitoare@petrobras.com.br', 'publicado(a)', 0),
(144, '', ' jorgenasc@petrobras.com.br', 'publicado(a)', 0),
(145, '', ' jcssantos@petrobras.com.br', 'publicado(a)', 0),
(146, '', ' benevaldo@petrobras.com.br', 'publicado(a)', 0),
(147, '', ' cardi@petrobras.com.br', 'publicado(a)', 0),
(148, '', ' joatan@petrobras.com.br', 'publicado(a)', 0),
(149, '', ' valneivasconcelos@petrobras.com.br', 'publicado(a)', 0),
(172, '', ' pvictorrocha@petrobras.com.br', 'publicado(a)', 0),
(173, '', ' robsonpessanha@petrobras.com.br', 'publicado(a)', 0),
(174, '', ' radav@petrobras.com.br', 'publicado(a)', 0),
(175, '', ' jlbarros@petrobras.com.br', 'publicado(a)', 0),
(176, '', ' fabiosales@petrobras.com.br', 'publicado(a)', 0),
(177, '', ' brunofrbr@petrobras.com.br', 'publicado(a)', 0),
(178, '', ' jua@petrobras.com.br', 'publicado(a)', 0),
(179, '', ' acorreia@petrobras.com.br', 'publicado(a)', 0),
(180, '', ' rfranco@petrobras.com.br', 'publicado(a)', 0),
(181, '', ' luizvicentini@petrobras.com.br', 'publicado(a)', 0),
(182, '', ' marcio.carneiro@petrobras.com.br', 'publicado(a)', 0),
(183, '', ' patricia.parente@petrobras.com.br', 'publicado(a)', 0),
(184, '', ' fabrifisica@petrobras.com.br', 'publicado(a)', 0),
(185, '', ' rcazevedo@petrobras.com.br', 'publicado(a)', 0),
(186, '', ' jrmadeira@petrobras.com.br', 'publicado(a)', 0),
(187, '', ' zeucunha@petrobras.com.br', 'publicado(a)', 0),
(188, '', ' rbtsandro.elet@petrobras.com.br', 'publicado(a)', 0),
(189, '', ' melquisedec@petrobras.com.br', 'publicado(a)', 0),
(190, '', ' pablotadeu@petrobras.com.br', 'publicado(a)', 0),
(191, '', ' claudio.bastos@petrobras.com.br', 'publicado(a)', 0),
(192, '', ' emanueleduarte@petrobras.com.br', 'publicado(a)', 0),
(193, '', ' eloizo@petrobras.com.br', 'publicado(a)', 0),
(194, '', ' lvieira@petrobras.com.br', 'publicado(a)', 0),
(195, '', ' lucena_pedro@petrobras.com.br', 'publicado(a)', 0),
(196, '', ' tarsoleandro@petrobras.com.br', 'publicado(a)', 0),
(197, '', ' mauriciossousa@petrobras.com.br', 'publicado(a)', 0),
(198, '', ' leandrobc@petrobras.com.br', 'publicado(a)', 0),
(199, '', ' meryhelen@petrobras.com.br', 'publicado(a)', 0),
(200, '', ' helenoteixeira@petrobras.com.br', 'publicado(a)', 0),
(201, '', ' rodrigo.rrs@petrobras.com.br', 'publicado(a)', 0),
(202, '', ' leonardo.alves@petrobras.com.br', 'publicado(a)', 0),
(203, '', ' carlsonmagno@petrobras.com.br', 'publicado(a)', 0),
(204, '', ' evaltovaz@petrobras.com.br', 'publicado(a)', 0),
(205, '', ' edersonlessa@petrobras.com.br', 'publicado(a)', 0),
(206, '', ' m.pereira@petrobras.com.br', 'publicado(a)', 0),
(207, '', ' cesar-silva@petrobras.com.br', 'publicado(a)', 0),
(208, '', ' fabiana.menezes@petrobras.com.br', 'publicado(a)', 0),
(209, '', ' m.alexandre@petrobras.com.br', 'publicado(a)', 0),
(210, '', ' marcelo.reis@petrobras.com.br', 'publicado(a)', 0),
(211, '', ' thiagoleal@petrobras.com.br', 'publicado(a)', 0),
(212, '', ' angelofonseca@petrobras.com.br', 'publicado(a)', 0),
(213, '', ' guerdi@petrobras.com.br', 'publicado(a)', 0),
(214, '', ' hudson.torquato@petrobras.com.br', 'publicado(a)', 0),
(215, '', ' manoelmsb@petrobras.com.br', 'publicado(a)', 0),
(216, '', ' educampos@petrobras.com.br', 'publicado(a)', 0),
(217, '', ' ofp_69@hotmail.com', 'publicado(a)', 0),
(218, '', ' marchi@petrobras.com.br', 'publicado(a)', 0),
(219, '', ' isnar_almeida@petrobras.com.br', 'publicado(a)', 0),
(220, '', ' gteixeira@petrobras.com.br', 'publicado(a)', 0),
(221, '', ' annibal@petrobras.com.br', 'publicado(a)', 0),
(222, '', ' jeff@petrobras.com.br', 'publicado(a)', 0),
(223, '', ' edilzia@petrobras.com.br', 'publicado(a)', 0),
(224, '', ' vothomaz@petrobras.com.br', 'publicado(a)', 0),
(225, '', ' mario_piazza@petrobras.com.br', 'publicado(a)', 0),
(226, '', ' gastaldo@petrobras.com.br', 'publicado(a)', 0),
(227, '', ' claudio_da_gata@petrobras.com.br', 'publicado(a)', 0),
(228, '', ' rcpad@petrobras.com.br', 'publicado(a)', 0),
(229, '', ' alberto.costa@petrobras.com.br', 'publicado(a)', 0),
(230, '', ' vergilio@petrobras.com.br', 'publicado(a)', 0),
(231, '', ' rbr@petrobras.com.br', 'publicado(a)', 0),
(232, '', ' semogmelo@petrobras.com.br', 'publicado(a)', 0),
(233, '', ' mariojorge@petrobras.com.br', 'publicado(a)', 0),
(234, '', ' nunoreboucas@petrobras.com.br', 'publicado(a)', 0),
(235, '', ' alvaroinacio@petrobras.com.br', 'publicado(a)', 0),
(236, '', ' renersouza@petrobras.com.br', 'publicado(a)', 0),
(237, '', ' olimpiomussi@petrobras.com.br', 'publicado(a)', 0),
(238, '', ' neircoutinho@petrobras.com.br', 'publicado(a)', 0),
(239, '', ' josuetavares@petrobras.com.br', 'publicado(a)', 0),
(240, '', ' ma.petersen@petrobras.com.br', 'publicado(a)', 0),
(241, '', ' rpmauricio@petrobras.com.br', 'publicado(a)', 0),
(242, '', ' rooy@petrobras.com.br', 'publicado(a)', 0),
(243, '', ' odilonsilva@petrobras.com.br', 'publicado(a)', 0),
(244, '', ' p.lima@petrobras.com.br', 'publicado(a)', 0),
(245, '', ' biro@petrobras.com.br', 'publicado(a)', 0),
(246, '', ' marcosantonio@petrobras.com.br', 'publicado(a)', 0),
(247, '', ' cvitor@petrobras.com.br', 'publicado(a)', 0),
(248, '', ' cmurilo@petrobras.com.br', 'publicado(a)', 0),
(249, '', ' jlmendonca@petrobras.com.br', 'publicado(a)', 0),
(250, '', ' vgvs@petrobras.com.br', 'publicado(a)', 0),
(251, '', ' legnar@petrobras.com.br', 'publicado(a)', 0),
(252, '', ' dalmir@petrobras.com.br', 'publicado(a)', 0),
(253, '', ' valdopa@petrobras.com.br', 'publicado(a)', 0),
(254, '', ' meviana@petrobras.com.br', 'publicado(a)', 0),
(255, '', ' joelsonrs@petrobras.com.br', 'publicado(a)', 0),
(256, '', ' dinizpaulo@petrobras.com.br', 'publicado(a)', 0),
(257, '', ' jmacedo@petrobras.com.br', 'publicado(a)', 0),
(258, '', ' mazinhoaf@petrobras.com.br', 'publicado(a)', 0),
(259, '', ' geomarvellemen@petrobras.com.br', 'publicado(a)', 0),
(260, '', ' lmesteves@petrobras.com.br', 'publicado(a)', 0),
(261, '', ' flaviosd@petrobras.com.br', 'publicado(a)', 0),
(262, '', ' lcsan@petrobras.com.br', 'publicado(a)', 0),
(263, '', ' operadormaia@petrobras.com.br', 'publicado(a)', 0),
(264, '', ' paulosr@petrobras.com.br', 'publicado(a)', 0),
(265, '', ' anesia@petrobras.com.br', 'publicado(a)', 0),
(266, '', ' spedro@petrobras.com.br', 'publicado(a)', 0),
(267, '', ' aamoreira@petrobras.com.br', 'publicado(a)', 0),
(268, '', ' ldionizio@petrobras.com.br', 'publicado(a)', 0),
(269, '', ' jurandirargolo@petrobras.com.br', 'publicado(a)', 0),
(270, '', ' mont@petrobras.com.br', 'publicado(a)', 0),
(271, '', ' joaottcruz@petrobras.com.br', 'publicado(a)', 0),
(272, '', ' edmo@petrobras.com.br', 'publicado(a)', 0),
(273, '', ' r.carlos@petrobras.com.br', 'publicado(a)', 0),
(274, '', ' josejesus@petrobras.com.br', 'publicado(a)', 0),
(275, '', ' jorge.cabral@petrobras.com.br', 'publicado(a)', 0),
(276, '', ' lcfcarneiro@petrobras.com.br', 'publicado(a)', 0),
(277, '', ' lfernando@petrobras.com.br', 'publicado(a)', 0),
(278, '', ' mdazevedo@petrobras.com.br', 'publicado(a)', 0),
(279, '', ' marcolan@petrobras.com.br', 'publicado(a)', 0),
(280, '', ' jquintana@petrobras.com.br', 'publicado(a)', 0),
(281, '', ' operadorsiqueira@petrobras.com.br', 'publicado(a)', 0),
(282, '', ' egamorim@petrobras.com.br', 'publicado(a)', 0),
(283, '', ' raimundo.santana@petrobras.com.br', 'publicado(a)', 0),
(284, '', ' ritamaria@petrobras.com.br', 'publicado(a)', 0),
(285, '', ' lgra@petrobras.com.br', 'publicado(a)', 0),
(286, '', ' osvaldosfilho@petrobras.com.br', 'publicado(a)', 0),
(287, '', ' sperandio@petrobras.com.br', 'publicado(a)', 0),
(288, '', ' gcabral@petrobras.com.br', 'publicado(a)', 0),
(289, '', ' mcop@petrobras.com.br', 'publicado(a)', 0),
(290, '', ' srsilvio@petrobras.com.br', 'publicado(a)', 0),
(291, '', ' dcsantoss@petrobras.com.br', 'publicado(a)', 0),
(292, '', ' opbs@petrobras.com.br', 'publicado(a)', 0),
(293, '', ' flavio.rodrigues@petrobras.com.br', 'publicado(a)', 0),
(294, '', ' eduardosoares@petrobras.com.br', 'publicado(a)', 0),
(295, '', ' jlhenrique@petrobras.com.br', 'publicado(a)', 0),
(296, '', ' sylviopacheco@petrobras.com.br', 'publicado(a)', 0),
(297, '', ' josevaldo@petrobras.com.br', 'publicado(a)', 0),
(298, '', ' digiorgio@petrobras.com.br', 'publicado(a)', 0),
(299, '', ' brunocampos@petrobras.com.br', 'publicado(a)', 0),
(300, '', ' diogorocha@petrobras.com.br', 'publicado(a)', 0),
(301, '', ' raquel.br@petrobras.com.br', 'publicado(a)', 0),
(302, '', ' escalmeida@petrobras.com.br', 'publicado(a)', 0),
(303, '', ' eduardowill@petrobras.com.br', 'publicado(a)', 0),
(304, '', ' louzada@petrobras.com.br', 'publicado(a)', 0),
(305, '', ' andre2003@petrobras.com.br', 'publicado(a)', 0),
(306, '', ' correamarcos@petrobras.com.br', 'publicado(a)', 0),
(307, '', ' carloscosta@petrobras.com.br', 'publicado(a)', 0),
(308, '', ' josylemos@petrobras.com.br', 'publicado(a)', 0),
(309, '', ' danialves@petrobras.com.br', 'publicado(a)', 0),
(310, '', ' andrevalbao@petrobras.com.br', 'publicado(a)', 0),
(311, '', ' ebmontenegro@petrobras.com.br', 'publicado(a)', 0),
(312, '', ' victorsuzano@petrobras.com.br', 'publicado(a)', 0),
(313, '', ' afersilva@petrobras.com.br', 'publicado(a)', 0),
(314, '', ' davimelo@petrobras.com.br', 'publicado(a)', 0),
(315, '', ' rafaelsp@petrobras.com.br', 'publicado(a)', 0),
(316, '', ' marciofsantos@petrobras.com.br', 'publicado(a)', 0),
(317, '', ' camaro@petrobras.com.br', 'publicado(a)', 0),
(318, '', ' caiocesars@petrobras.com.br', 'publicado(a)', 0),
(319, '', ' man16@petrobras.com.br', 'publicado(a)', 0),
(320, '', ' gil.lisboa@petrobras.com.br', 'publicado(a)', 0),
(321, '', ' niltonfm@petrobras.com.br', 'publicado(a)', 0),
(322, '', ' robertopt@petrobras.com.br', 'publicado(a)', 0),
(323, '', ' roquecruz@petrobras.com.br', 'publicado(a)', 0),
(324, '', ' romulomr@petrobras.com.br', 'publicado(a)', 0),
(325, '', ' carma@petrobras.com.br', 'publicado(a)', 0),
(326, '', ' aurelito@petrobras.com.br', 'publicado(a)', 0),
(327, '', ' almirfilho@petrobras.com.br', 'publicado(a)', 0),
(328, '', ' adroaldo@petrobras.com.br', 'publicado(a)', 0),
(329, '', ' urko@petrobras.com.br', 'publicado(a)', 0),
(330, '', ' sergiosantana@petrobras.com.br', 'publicado(a)', 0),
(331, '', ' robispo@petrobras.com.br', 'publicado(a)', 0),
(332, '', ' moissan@petrobras.com.br', 'publicado(a)', 0),
(333, '', ' esojr@petrobras.com.br', 'publicado(a)', 0),
(334, '', ' axavier@petrobras.com.br', 'publicado(a)', 0),
(335, '', ' rluis@petrobras.com.br', 'publicado(a)', 0),
(336, '', ' josinilton@petrobras.com.br', 'publicado(a)', 0),
(337, '', ' ripardoseg@petrobras.com.br', 'publicado(a)', 0),
(338, '', ' anmoura@petrobras.com.br', 'publicado(a)', 0),
(339, '', ' jairmagal@petrobras.com.br', 'publicado(a)', 0),
(340, '', ' aoa@petrobras.com.br', 'publicado(a)', 0),
(341, '', ' wellingtonfs@petrobras.com.br', 'publicado(a)', 0),
(342, '', ' pithon@petrobras.com.br', 'publicado(a)', 0),
(343, '', ' canela@petrobras.com.br', 'publicado(a)', 0),
(344, '', ' jwsfreitas@petrobras.com.br', 'publicado(a)', 0),
(345, '', ' fpaulino@petrobras.com.br', 'publicado(a)', 0),
(346, '', ' edimendo@petrobras.com.br', 'publicado(a)', 0),
(347, '', ' eduardomelo@petrobras.com.br', 'publicado(a)', 0),
(348, '', ' edpimentel@petrobras.com.br', 'publicado(a)', 0),
(349, '', ' aberildo@petrobras.com.br', 'publicado(a)', 0),
(350, '', ' robcarlos@petrobras.com.br', 'publicado(a)', 0),
(351, '', ' ckrimberg@petrobras.com.br', 'publicado(a)', 0),
(352, '', ' guerrieric@petrobras.com.br', 'publicado(a)', 0),
(353, '', ' paulolobo@petrobras.com.br', 'publicado(a)', 0),
(354, '', ' ivonildosantana@petrobras.com.br', 'publicado(a)', 0),
(355, '', ' wilbrito@petrobras.com.br', 'publicado(a)', 0),
(356, '', ' lujas@petrobras.com.br', 'publicado(a)', 0),
(357, '', ' marceloborgatto@petrobras.com.br', 'publicado(a)', 0),
(358, '', ' fabiopazetto@petrobras.com.br', 'publicado(a)', 0),
(359, '', ' carlos_nascimento@petrobras.com.br', 'publicado(a)', 0),
(360, '', ' mmfilho@petrobras.com.br', 'publicado(a)', 0),
(361, '', ' eliseutosetto@petrobras.com.br', 'publicado(a)', 0),
(362, '', ' benedito@petrobras.com.br', 'publicado(a)', 0),
(363, '', ' ronasantos@petrobras.com.br', 'publicado(a)', 0),
(364, '', ' jalbino@petrobras.com.br', 'publicado(a)', 0),
(365, '', ' heliofil@petrobras.com.br', 'publicado(a)', 0),
(366, '', ' aurelio@petrobras.com.br', 'publicado(a)', 0),
(367, '', ' antonio.fernando@petrobras.com.br', 'publicado(a)', 0),
(368, '', ' msjacob@petrobras.com.br', 'publicado(a)', 0),
(369, '', ' humbertosantos@petrobras.com.br', 'publicado(a)', 0),
(370, '', ' jfmesquita@petrobras.com.br', 'publicado(a)', 0),
(371, '', ' luizleal@petrobras.com.br', 'publicado(a)', 0),
(372, '', ' j_vieira@petrobras.com.br', 'publicado(a)', 0),
(373, '', ' anjosferreira@petrobras.com.br', 'publicado(a)', 0),
(374, '', ' cmtbarbosa@petrobras.com.br', 'publicado(a)', 0),
(375, '', ' micorrea@petrobras.com.br', 'publicado(a)', 0),
(376, '', ' julioviana@petrobras.com.br', 'publicado(a)', 0),
(377, '', ' wemar@petrobras.com.br', 'publicado(a)', 0),
(378, '', ' monte@petrobras.com.br', 'publicado(a)', 0),
(379, '', ' ary.moreira@petrobras.com.br', 'publicado(a)', 0),
(380, '', ' decarvalho@petrobras.com.br', 'publicado(a)', 0),
(381, '', ' wsalles@petrobras.com.br', 'publicado(a)', 0),
(382, '', ' jcbalves@petrobras.com.br', 'publicado(a)', 0),
(383, '', ' eafragoso@petrobras.com.br', 'publicado(a)', 0),
(384, '', ' jrmendes@petrobras.com.br', 'publicado(a)', 0),
(385, '', ' valmarpessanha@petrobras.com.br', 'publicado(a)', 0),
(386, '', ' mdvidal@petrobras.com.br', 'publicado(a)', 0),
(387, '', ' aristotelesvitor@petrobras.com.br', 'publicado(a)', 0),
(388, '', ' geraldolino@petrobras.com.br', 'publicado(a)', 0),
(389, '', ' cmvieira@petrobras.com.br', 'publicado(a)', 0),
(390, '', ' pmauro@petrobras.com.br', 'publicado(a)', 0),
(391, '', ' danielwillian@petrobras.com.br', 'publicado(a)', 0),
(392, '', ' joaolvm@petrobras.com.br', 'publicado(a)', 0),
(393, '', ' pfalves@petrobras.com.br', 'publicado(a)', 0),
(394, '', ' eliezerpm@petrobras.com.br', 'publicado(a)', 0),
(395, '', ' cribarcellos@petrobras.com.br', 'publicado(a)', 0),
(396, '', ' siberico@petrobras.com.br', 'publicado(a)', 0),
(397, '', ' crobsantos@petrobras.com.br', 'publicado(a)', 0),
(398, '', ' igor.anselmo@petrobras.com.br', 'publicado(a)', 0),
(399, '', ' mondequi@petrobras.com.br', 'publicado(a)', 0),
(400, '', ' antonio.tjb@petrobras.com.br', 'publicado(a)', 0),
(401, '', ' mauriciopimenta@petrobras.com.br', 'publicado(a)', 0),
(402, '', ' fabiogalvao@petrobras.com.br', 'publicado(a)', 0),
(403, '', ' viniciuspc@petrobras.com.br', 'publicado(a)', 0),
(404, '', ' marcand@petrobras.com.br', 'publicado(a)', 0),
(405, '', ' emirrogsilva@petrobras.com.br', 'publicado(a)', 0),
(406, '', ' mauriciombatista@petrobras.com.br', 'publicado(a)', 0),
(407, '', ' josilio@petrobras.com.br', 'publicado(a)', 0),
(408, '', ' mgreique@petrobras.com.br', 'publicado(a)', 0),
(409, '', ' bruno.n@petrobras.com.br', 'publicado(a)', 0),
(410, '', ' ', 'publicado(a)', 0),
(411, '', ' rsmoraes@petrobras.com.br', 'publicado(a)', 0),
(412, '', ' henrique.conceicao@petrobras.com.br', 'publicado(a)', 0),
(413, '', ' wvasconcelos@petrobras.com.br', 'publicado(a)', 0),
(414, '', ' ramalho.cm@petrobras.com.br', 'publicado(a)', 0),
(415, '', ' caliman@petrobras.com.br', 'publicado(a)', 0),
(416, '', ' marlos@petrobras.com.br', 'publicado(a)', 0),
(417, '', ' pgoldfeld@petrobras.com.br', 'publicado(a)', 0),
(418, '', ' fernandovidal@petrobras.com.br', 'publicado(a)', 0),
(419, '', ' nljunior@petrobras.com.br', 'publicado(a)', 0),
(420, '', ' lrios@petrobras.com.br', 'publicado(a)', 0),
(421, '', ' dogogm@petrobras.com.br', 'publicado(a)', 0),
(422, '', ' carloschristi@petrobras.com.br', 'publicado(a)', 0),
(423, '', ' dan.peres@petrobras.com.br', 'publicado(a)', 0),
(424, '', ' maluiza@petrobras.com.br', 'publicado(a)', 0),
(425, '', ' flvms@petrobras.com.br', 'publicado(a)', 0),
(426, '', ' ronaldovicente@petrobras.com.br', 'publicado(a)', 0),
(427, '', ' carolinasilveira@petrobras.com.br', 'publicado(a)', 0),
(428, '', ' dalfradique@petrobras.com.br', 'publicado(a)', 0),
(429, '', ' edioneto@petrobras.com.br', 'publicado(a)', 0),
(430, '', ' thiagorp@petrobras.com.br', 'publicado(a)', 0),
(431, '', ' joaododde@petrobras.com.br', 'publicado(a)', 0),
(432, '', ' ivandropereira@petrobras.com.br', 'publicado(a)', 0),
(433, '', ' wagnergoulart@petrobras.com.br', 'publicado(a)', 0),
(434, '', ' periclesfaioli@petrobras.com.br', 'publicado(a)', 0),
(435, '', ' charlesvb@petrobras.com.br', 'publicado(a)', 0),
(436, '', ' vladimirvianna@petrobras.com.br', 'publicado(a)', 0),
(437, '', ' ivanlandrade@petrobras.com.br', 'publicado(a)', 0),
(438, '', ' lnsilva@petrobras.com.br', 'publicado(a)', 0),
(439, '', ' rodbras@petrobras.com.br', 'publicado(a)', 0),
(440, '', ' menegati@petrobras.com.br', 'publicado(a)', 0),
(441, '', ' thiagofontoura@petrobras.com.br', 'publicado(a)', 0),
(442, '', ' cjramos@petrobras.com.br', 'publicado(a)', 0),
(443, '', ' contrap@petrobras.com.br', 'publicado(a)', 0),
(444, '', ' wendelsc@petrobras.com.br', 'publicado(a)', 0),
(445, '', ' thiagofonte@petrobras.com.br', 'publicado(a)', 0),
(446, '', ' jlmorais@petrobras.com.br', 'publicado(a)', 0),
(447, '', ' thiagomec@petrobras.com.br', 'publicado(a)', 0),
(448, '', ' geraldo.sa.santos@petrobras.com.br', 'publicado(a)', 0),
(449, '', ' wtlsoares@petrobras.com.br', 'publicado(a)', 0),
(450, '', ' romildofreitas@petrobras.com.br', 'publicado(a)', 0),
(451, '', ' marcelomoitinho@petrobras.com.br', 'publicado(a)', 0),
(452, '', ' gabrielssantos@petrobras.com.br', 'publicado(a)', 0),
(453, '', ' alvimar.f@petrobras.com.br', 'publicado(a)', 0),
(454, '', ' nilmar@petrobras.com.br', 'publicado(a)', 0),
(455, '', ' mapimentel@petrobras.com.br', 'publicado(a)', 0),
(456, '', ' barbudo@petrobras.com.br', 'publicado(a)', 0),
(457, '', ' oseiasmoraes@petrobras.com.br', 'publicado(a)', 0),
(458, '', ' reinaldodias@petrobras.com.br', 'publicado(a)', 0),
(459, '', ' altizana@petrobras.com.br', 'publicado(a)', 0),
(460, '', ' ernestho@petrobras.com.br', 'publicado(a)', 0),
(461, '', ' paulohgc@petrobras.com.br', 'publicado(a)', 0),
(462, '', ' isaiasmeireles@petrobras.com.br', 'publicado(a)', 0),
(463, '', ' neimares@petrobras.com.br', 'publicado(a)', 0),
(464, '', ' juliana.germiniani@petrobras.com.br', 'publicado(a)', 0),
(465, '', ' paulopontes@petrobras.com.br', 'publicado(a)', 0),
(466, '', ' alancastro@petrobras.com.br', 'publicado(a)', 0),
(467, '', ' jescobar@petrobras.com.br', 'publicado(a)', 0),
(468, '', ' a.marcos@petrobras.com.br', 'publicado(a)', 0),
(469, '', ' maiconcarvalho@petrobras.com.br', 'publicado(a)', 0),
(470, '', ' digaotop@petrobras.com.br', 'publicado(a)', 0),
(471, '', ' victordeandrade@petrobras.com.br', 'publicado(a)', 0),
(472, '', ' flscosta@petrobras.com.br', 'publicado(a)', 0),
(473, '', ' alexandretavares@petrobras.com.br', 'publicado(a)', 0),
(474, '', ' gustavotrott@petrobras.com.br', 'publicado(a)', 0),
(475, '', ' sales_junior@petrobras.com.br', 'publicado(a)', 0),
(476, '', ' nelsoneto@petrobras.com.br', 'publicado(a)', 0),
(477, '', ' lcperes@petrobras.com.br', 'publicado(a)', 0),
(478, '', ' ajn@petrobras.com.br', 'publicado(a)', 0),
(479, '', ' lcloures@petrobras.com.br', 'publicado(a)', 0),
(480, '', ' rmnogueira@petrobras.com.br', 'publicado(a)', 0),
(481, '', ' ernani.dias@petrobras.com.br', 'publicado(a)', 0),
(482, '', ' breno.vasconcelos@petrobras.com.br', 'publicado(a)', 0),
(483, '', ' l.antunes@petrobras.com.br', 'publicado(a)', 0),
(484, '', ' silvajunior@petrobras.com.br', 'publicado(a)', 0),
(485, '', ' sergiojosepignaton@petrobras.com.br', 'publicado(a)', 0),
(486, '', ' marcelvictor@petrobras.com.br', 'publicado(a)', 0),
(487, '', ' lmaia@petrobras.com.br', 'publicado(a)', 0),
(488, '', ' zleite@petrobras.com.br', 'publicado(a)', 0),
(489, '', ' danielle.lopes@petrobras.com.br', 'publicado(a)', 0),
(490, '', ' sdasilva@petrobras.com.br', 'publicado(a)', 0),
(491, '', ' claudio_neres@petrobras.com.br', 'publicado(a)', 0),
(492, '', ' ofp_69@hotmail.com', 'publicado(a)', 0),
(493, '', ' c.pestana@petrobras.com.br', 'publicado(a)', 0),
(494, '', ' agospoim@petrobras.com.br', 'publicado(a)', 0),
(495, '', ' agajunior@petrobras.com.br', 'publicado(a)', 0),
(496, '', ' nicolaos@petrobras.com.br', 'publicado(a)', 0),
(497, '', ' sanderbh@petrobras.com.br', 'publicado(a)', 0),
(498, '', ' daniel.lopes@petrobras.com.br', 'publicado(a)', 0),
(499, '', ' hans@petrobras.com.br', 'publicado(a)', 0),
(500, '', ' edno@petrobras.com.br', 'publicado(a)', 0),
(501, '', ' cbs43@petrobras.com.br', 'publicado(a)', 0),
(502, '', ' maury@petrobras.com.br', 'publicado(a)', 0),
(503, '', ' hoazevedo@petrobras.com.br', 'publicado(a)', 0),
(504, '', ' ernanilf@petrobras.com.br', 'publicado(a)', 0),
(505, '', ' jcjesus@petrobras.com.br', 'publicado(a)', 0),
(506, '', ' mariadaconceicao@petrobras.com.br', 'publicado(a)', 0),
(507, '', ' gilsonpavao@petrobras.com.br', 'publicado(a)', 0),
(508, '', ' pbarros@petrobras.com.br', 'publicado(a)', 0),
(509, '', ' antoniodias@petrobras.com.br', 'publicado(a)', 0),
(510, '', ' renatotinoco@petrobras.com.br', 'publicado(a)', 0),
(511, '', ' alfa01@petrobras.com.br', 'publicado(a)', 0),
(512, '', ' nasci@petrobras.com.br', 'publicado(a)', 0),
(513, '', ' almircoutinho@petrobras.com.br', 'publicado(a)', 0),
(514, '', ' msbarros@petrobras.com.br', 'publicado(a)', 0),
(515, '', ' clementeloterio@petrobras.com.br', 'publicado(a)', 0),
(516, '', ' cleo.melo@petrobras.com.br', 'publicado(a)', 0),
(517, '', ' neilson@petrobras.com.br', 'publicado(a)', 0),
(518, '', ' rojala@petrobras.com.br', 'publicado(a)', 0),
(519, '', ' rpmachado@petrobras.com.br', 'publicado(a)', 0),
(520, '', ' ptsilveira@petrobras.com.br', 'publicado(a)', 0),
(521, '', ' oscar.filho@petrobras.com.br', 'publicado(a)', 0),
(522, '', ' mromero@petrobras.com.br', 'publicado(a)', 0),
(523, '', ' npaiva@petrobras.com.br', 'publicado(a)', 0),
(524, '', ' jorgecar@petrobras.com.br', 'publicado(a)', 0),
(525, '', ' edureis@petrobras.com.br', 'publicado(a)', 0),
(526, '', ' gherardi@petrobras.com.br', 'publicado(a)', 0),
(527, '', ' dalgio@petrobras.com.br', 'publicado(a)', 0),
(528, '', ' mary@petrobras.com.br', 'publicado(a)', 0),
(529, '', ' geilsonguimaraes@petrobras.com.br', 'publicado(a)', 0),
(530, '', ' everaldofilho@petrobras.com.br', 'publicado(a)', 0),
(531, '', ' xavier@petrobras.com.br', 'publicado(a)', 0),
(532, '', ' edmaramc@petrobras.com.br', 'publicado(a)', 0),
(533, '', ' antunesac@petrobras.com.br', 'publicado(a)', 0),
(534, '', ' w_alvarenga@petrobras.com.br', 'publicado(a)', 0),
(535, '', ' bebel@petrobras.com.br', 'publicado(a)', 0),
(536, '', ' fernan2@petrobras.com.br', 'publicado(a)', 0),
(537, '', ' luisrosa@petrobras.com.br', 'publicado(a)', 0),
(538, '', ' lafalves@petrobras.com.br', 'publicado(a)', 0),
(539, '', ' aveloso@petrobras.com.br', 'publicado(a)', 0),
(540, '', ' renatojsantos@petrobras.com.br', 'publicado(a)', 0),
(541, '', ' ricardopc@petrobras.com.br', 'publicado(a)', 0),
(542, '', ' dlsouza@petrobras.com.br', 'publicado(a)', 0),
(543, '', ' erikacrespo@petrobras.com.br', 'publicado(a)', 0),
(544, '', ' alexsander@petrobras.com.br', 'publicado(a)', 0),
(545, '', ' carlos.renato@petrobras.com.br', 'publicado(a)', 0),
(546, '', ' aerton@petrobras.com.br', 'publicado(a)', 0),
(547, '', ' vanessapm@petrobras.com.br', 'publicado(a)', 0),
(548, '', ' josenildoav@petrobras.com.br', 'publicado(a)', 0),
(549, '', ' belmont@petrobras.com.br', 'publicado(a)', 0),
(550, '', ' carogio@petrobras.com.br', 'publicado(a)', 0),
(551, '', ' renatoramos@petrobras.com.br', 'publicado(a)', 0),
(552, '', ' robalo@petrobras.com.br', 'publicado(a)', 0),
(553, '', ' heberanizio@petrobras.com.br', 'publicado(a)', 0),
(554, '', ' hugosartori@petrobras.com.br', 'publicado(a)', 0),
(555, '', ' edlferraz@petrobras.com.br', 'publicado(a)', 0),
(556, '', ' jccmattos@petrobras.com.br', 'publicado(a)', 0),
(557, '', ' desousa@petrobras.com.br', 'publicado(a)', 0),
(558, '', ' vitor.leite@petrobras.com.br', 'publicado(a)', 0),
(559, '', ' robertasl@petrobras.com.br', 'publicado(a)', 0),
(560, '', ' peixoto@petrobras.com.br', 'publicado(a)', 0),
(561, '', ' tsqueiroz@petrobras.com.br', 'publicado(a)', 0),
(562, '', ' jfleal@petrobras.com.br', 'publicado(a)', 0),
(563, '', ' fcoleite@petrobras.com.br', 'publicado(a)', 0),
(564, '', ' klebersp@petrobras.com.br', 'publicado(a)', 0),
(565, '', ' vitorrangel@petrobras.com.br', 'publicado(a)', 0),
(566, '', ' robertoamorim@petrobras.com.br', 'publicado(a)', 0),
(567, '', ' clebersalles@petrobras.com.br', 'publicado(a)', 0),
(568, '', ' arqlima@petrobras.com.br', 'publicado(a)', 0),
(569, '', ' sidarta@petrobras.com.br', 'publicado(a)', 0),
(570, '', ' mbsantos@petrobras.com.br', 'publicado(a)', 0),
(571, '', ' jguima@petrobras.com.br', 'publicado(a)', 0),
(572, '', ' jclgel@petrobras.com.br', 'publicado(a)', 0),
(573, '', ' mjalmeida@petrobras.com.br', 'publicado(a)', 0),
(574, '', ' menezes@petrobras.com.br', 'publicado(a)', 0),
(575, '', ' claudiofranca@petrobras.com.br', 'publicado(a)', 0),
(576, '', ' alexcarneiro@petrobras.com.br', 'publicado(a)', 0),
(577, '', ' cristovaomoura@petrobras.com.br', 'publicado(a)', 0),
(578, '', ' teonio@petrobras.com.br', 'publicado(a)', 0),
(579, '', ' r.rabelo@petrobras.com.br', 'publicado(a)', 0),
(580, '', ' otamar@petrobras.com.br', 'publicado(a)', 0),
(581, '', ' francinaldoaf@petrobras.com.br', 'publicado(a)', 0),
(582, '', ' magnusfonseca@petrobras.com.br', 'publicado(a)', 0),
(583, '', ' valdick@petrobras.com.br', 'publicado(a)', 0),
(584, '', ' arcanjom@petrobras.com.br', 'publicado(a)', 0),
(585, '', ' ruymiguel@petrobras.com.br', 'publicado(a)', 0),
(586, '', ' gilsonbarbosa@petrobras.com.br', 'publicado(a)', 0),
(587, '', ' ronaldoreis@petrobras.com.br', 'publicado(a)', 0),
(588, '', ' gcpaz@petrobras.com.br', 'publicado(a)', 0),
(589, '', ' mfeitoza@petrobras.com.br', 'publicado(a)', 0),
(590, '', ' vicentneto@petrobras.com.br', 'publicado(a)', 0),
(591, '', ' jailton_silva@petrobras.com.br', 'publicado(a)', 0),
(592, '', ' juviniano@petrobras.com.br', 'publicado(a)', 0),
(593, '', ' l_augusto@petrobras.com.br', 'publicado(a)', 0),
(594, '', ' c.cerqueira@petrobras.com.br', 'publicado(a)', 0),
(595, '', ' waldson@petrobras.com.br', 'publicado(a)', 0),
(596, '', ' armiranda@petrobras.com.br', 'publicado(a)', 0),
(597, '', ' pin8551@petrobras.com.br', 'publicado(a)', 0),
(598, '', ' salutinunes@petrobras.com.br', 'publicado(a)', 0),
(599, '', ' caique@petrobras.com.br', 'publicado(a)', 0),
(600, '', ' orenstein@petrobras.com.br', 'publicado(a)', 0),
(601, '', ' cesargaya@petrobras.com.br', 'publicado(a)', 0),
(602, '', ' henaut@petrobras.com.br', 'publicado(a)', 0),
(603, '', ' douglasalessandro@petrobras.com.br', 'publicado(a)', 0),
(604, '', ' vladiassis@petrobras.com.br', 'publicado(a)', 0),
(605, '', ' isbahia@petrobras.com.br', 'publicado(a)', 0),
(606, '', ' fabricio_torres@petrobras.com.br', 'publicado(a)', 0),
(607, '', ' ricardohenrique@petrobras.com.br', 'publicado(a)', 0),
(608, '', ' alexsandrops@petrobras.com.br', 'publicado(a)', 0),
(609, '', ' juanes@petrobras.com.br', 'publicado(a)', 0),
(610, '', ' lindberger@petrobras.com.br', 'publicado(a)', 0),
(611, '', ' marciomoreno@petrobras.com.br', 'publicado(a)', 0),
(612, '', '  ronaldrangel@petrobras.com.br', 'publicado(a)', 0),
(613, '', ' andersontimoteo@petrobras.com.br', 'publicado(a)', 0),
(614, '', ' alexandre.fso@petrobras.com.br', 'publicado(a)', 0),
(615, '', ' deivisson@petrobras.com.br', 'publicado(a)', 0),
(616, '', ' renatolt@petrobras.com.br', 'publicado(a)', 0),
(617, '', ' vitalino@petrobras.com.br', 'publicado(a)', 0),
(618, '', ' mao@petrobras.com.br', 'publicado(a)', 0),
(619, '', ' tcezana@petrobras.com.br', 'publicado(a)', 0),
(620, '', ' jeanmichael@petrobras.com.br', 'publicado(a)', 0),
(621, '', ' flaviosampaio@petrobras.com.br', 'publicado(a)', 0),
(622, '', ' eduardosartori@petrobras.com.br', 'publicado(a)', 0),
(623, '', ' jovani@petrobras.com.br', 'publicado(a)', 0),
(624, '', ' joaoekarla@petrobras.com.br', 'publicado(a)', 0),
(625, '', ' renatocmoraes@petrobras.com.br', 'publicado(a)', 0),
(626, '', ' danielcabral@petrobras.com.br', 'publicado(a)', 0),
(627, '', ' wagneribeiro@petrobras.com.br', 'publicado(a)', 0),
(628, '', ' vitorstelmann@petrobras.com.br', 'publicado(a)', 0),
(629, '', ' lucascd@petrobras.com.br', 'publicado(a)', 0),
(630, '', ' valdeci.junior@petrobras.com.br', 'publicado(a)', 0),
(631, '', ' ricardoaugustino@petrobras.com.br', 'publicado(a)', 0),
(632, '', ' yurialvarenga@petrobras.com.br', 'publicado(a)', 0),
(633, '', ' luciano.vitor@petrobras.com.br', 'publicado(a)', 0),
(634, '', ' lennon@petrobras.com.br', 'publicado(a)', 0),
(635, '', ' leonanmarchi@petrobras.com.br', 'publicado(a)', 0),
(636, '', ' quimicaroll@petrobras.com.br', 'publicado(a)', 0),
(637, '', ' richardsonmo@petrobras.com.br', 'publicado(a)', 0),
(638, '', ' marialourdesfbj@petrobras.com.br', 'publicado(a)', 0),
(639, '', ' erickjunqueira@petrobras.com.br', 'publicado(a)', 0),
(640, '', ' flaviotomelin@petrobras.com.br', 'publicado(a)', 0),
(641, '', ' julciley@petrobras.com.br', 'publicado(a)', 0),
(642, '', ' wembley@petrobras.com.br', 'publicado(a)', 0),
(643, '', ' ruiacf@petrobras.com.br', 'publicado(a)', 0),
(644, '', ' gabrielmotta@petrobras.com.br', 'publicado(a)', 0),
(645, '', ' luizcordeiro@petrobras.com.br', 'publicado(a)', 0),
(646, '', ' luizcordeiro@petrobras.com.br', 'publicado(a)', 0),
(647, '', ' maxvinicius@petrobras.com.br', 'publicado(a)', 0),
(648, '', ' mcelo_sant@petrobras.com.br', 'publicado(a)', 0),
(649, '', ' rocruz@petrobras.com.br', 'publicado(a)', 0),
(650, '', ' leandrosilopes@petrobras.com.br', 'publicado(a)', 0),
(651, '', ' mbfer@petrobras.com.br', 'publicado(a)', 0),
(652, '', ' wccampos@petrobras.com.br', 'publicado(a)', 0),
(653, '', ' felipe.ferreira@petrobras.com.br', 'publicado(a)', 0),
(654, '', ' cleverdp@petrobras.com.br', 'publicado(a)', 0),
(655, '', ' leonardoabreu@petrobras.com.br', 'publicado(a)', 0),
(656, '', ' carlosmdias@petrobras.com.br', 'publicado(a)', 0),
(657, '', ' thiagommuniz@petrobras.com.br', 'publicado(a)', 0),
(658, '', ' wilmarlk@petrobras.com.br', 'publicado(a)', 0),
(659, '', ' laren@petrobras.com.br', 'publicado(a)', 0),
(660, '', ' ernandopetniunas@petrobras.com.br', 'publicado(a)', 0),
(661, '', ' edimilson.vaz@petrobras.com.br', 'publicado(a)', 0),
(662, '', ' ofp_69@hotmail.com', 'publicado(a)', 0),
(663, '', ' pfrosa@petrobras.com.br', 'publicado(a)', 0),
(664, '', ' carlostavares@petrobras.com.br', 'publicado(a)', 0),
(665, '', ' ebenezermarques@petrobras.com.br', 'publicado(a)', 0),
(666, '', ' rogerio_pereira@petrobras.com.br', 'publicado(a)', 0),
(667, '', ' proner@petrobras.com.br', 'publicado(a)', 0),
(668, '', ' clauder@petrobras.com.br', 'publicado(a)', 0),
(669, '', ' bagoncalves@petrobras.com.br', 'publicado(a)', 0),
(670, '', ' luizotaviom@petrobras.com.br', 'publicado(a)', 0),
(671, '', ' paulorl@petrobras.com.br', 'publicado(a)', 0),
(672, '', ' nedercabral@petrobras.com.br', 'publicado(a)', 0),
(673, '', ' acaran@petrobras.com.br', 'publicado(a)', 0),
(674, '', ' melgomes@petrobras.com.br', 'publicado(a)', 0),
(675, '', ' robertogarcia@petrobras.com.br', 'publicado(a)', 0),
(676, '', ' levyjr@petrobras.com.br', 'publicado(a)', 0),
(677, '', ' edlobo@petrobras.com.br', 'publicado(a)', 0),
(678, '', ' edcabral@petrobras.com.br', 'publicado(a)', 0),
(679, '', ' dejaime@petrobras.com.br', 'publicado(a)', 0),
(680, '', ' asclemente@petrobras.com.br', 'publicado(a)', 0),
(681, '', ' fernandojmsouza@petrobras.com.br', 'publicado(a)', 0),
(682, '', ' alwagner@petrobras.com.br', 'publicado(a)', 0),
(683, '', ' luiscataldo@petrobras.com.br', 'publicado(a)', 0),
(684, '', ' williamslr@petrobras.com.br', 'publicado(a)', 0),
(685, '', ' rodriguesac@petrobras.com.br', 'publicado(a)', 0),
(686, '', ' guiac@petrobras.com.br', 'publicado(a)', 0),
(687, '', ' fernandosouza@petrobras.com.br', 'publicado(a)', 0),
(688, '', ' capila@petrobras.com.br', 'publicado(a)', 0),
(689, '', ' terry@petrobras.com.br', 'publicado(a)', 0),
(690, '', ' limabastos@petrobras.com.br', 'publicado(a)', 0),
(691, '', ' rosanacampos@petrobras.com.br', 'publicado(a)', 0),
(692, '', ' rlouro@petrobras.com.br', 'publicado(a)', 0),
(693, '', ' eduardov@petrobras.com.br', 'publicado(a)', 0),
(694, '', ' cvjunior@petrobras.com.br', 'publicado(a)', 0),
(695, '', ' santauro@petrobras.com.br', 'publicado(a)', 0),
(696, '', ' lecques@petrobras.com.br', 'publicado(a)', 0),
(697, '', ' jadirb@petrobras.com.br', 'publicado(a)', 0),
(698, '', ' neydomingues@petrobras.com.br', 'publicado(a)', 0),
(699, '', ' marcoslobo@petrobras.com.br', 'publicado(a)', 0),
(700, '', ' natonunes@petrobras.com.br', 'publicado(a)', 0),
(701, '', ' reginacostarangel@petrobras.com.br', 'publicado(a)', 0),
(702, '', ' bravin@petrobras.com.br', 'publicado(a)', 0),
(703, '', ' franciso@petrobras.com.br', 'publicado(a)', 0),
(704, '', ' amantunes@petrobras.com.br', 'publicado(a)', 0),
(705, '', ' ram@petrobras.com.br', 'publicado(a)', 0),
(706, '', ' cjtinoco@petrobras.com.br', 'publicado(a)', 0),
(707, '', ' orleansl@petrobras.com.br', 'publicado(a)', 0),
(708, '', ' oswaldofpinto@petrobras.com.br', 'publicado(a)', 0),
(709, '', ' hermesnj@petrobras.com.br', 'publicado(a)', 0),
(710, '', ' ediochrist@petrobras.com.br', 'publicado(a)', 0),
(711, '', ' riscadoa@petrobras.com.br', 'publicado(a)', 0),
(712, '', ' fbelarmino@petrobras.com.br', 'publicado(a)', 0),
(713, '', ' lucianess@petrobras.com.br', 'publicado(a)', 0),
(714, '', ' leoreis@petrobras.com.br', 'publicado(a)', 0),
(715, '', ' ricsantos@petrobras.com.br', 'publicado(a)', 0),
(716, '', ' robsoncampos@petrobras.com.br', 'publicado(a)', 0),
(717, '', ' evandrogomes@petrobras.com.br', 'publicado(a)', 0),
(718, '', ' lucianamuylaert@petrobras.com.br', 'publicado(a)', 0),
(719, '', ' mcosta@petrobras.com.br', 'publicado(a)', 0),
(720, '', ' andrerj@petrobras.com.br', 'publicado(a)', 0),
(721, '', ' rogeriosilva@petrobras.com.br', 'publicado(a)', 0),
(722, '', ' plemos@petrobras.com.br', 'publicado(a)', 0),
(723, '', ' filipeboechat@petrobras.com.br', 'publicado(a)', 0),
(724, '', ' robsoncouto@petrobras.com.br, lfosodre@petrobras.com.br', 'publicado(a)', 0),
(725, '', ' alerlima@petrobras.com.br', 'publicado(a)', 0),
(726, '', ' evanio@petrobras.com.br', 'publicado(a)', 0),
(727, '', ' amtf@petrobras.com.br', 'publicado(a)', 0),
(728, '', ' fabiojpinheiro@petrobras.com.br', 'publicado(a)', 0),
(729, '', ' kasak@petrobras.com.br', 'publicado(a)', 0),
(730, '', ' douglas01@petrobras.com.br', 'publicado(a)', 0),
(731, '', ' felipe.mpc@petrobras.com.br', 'publicado(a)', 0),
(732, '', ' marcinho@petrobras.com.br', 'publicado(a)', 0),
(733, '', ' armandojp@petrobras.com.br', 'publicado(a)', 0),
(734, '', ' leonardoval@petrobras.com.br', 'publicado(a)', 0),
(735, '', ' teles@petrobras.com.br', 'publicado(a)', 0),
(736, '', ' amacar@petrobras.com.br', 'publicado(a)', 0),
(737, '', ' dematos@petrobras.com.br', 'publicado(a)', 0),
(738, '', ' marcio1@petrobras.com.br', 'publicado(a)', 0),
(739, '', ' porto@petrobras.com.br', 'publicado(a)', 0),
(740, '', ' jotaferreira@petrobras.com.br', 'publicado(a)', 0),
(741, '', ' olavo.pinheiro@petrobras.com.br', 'publicado(a)', 0),
(742, '', ' fbira@petrobras.com.br', 'publicado(a)', 0),
(743, '', ' claudiodantas@petrobras.com.br', 'publicado(a)', 0),
(744, '', ' jose.claudio@petrobras.com.br', 'publicado(a)', 0),
(745, '', ' zemarcos33@petrobras.com.br', 'publicado(a)', 0),
(746, '', ' darlex@petrobras.com.br', 'publicado(a)', 0),
(747, '', ' gutembergribeiro@petrobras.com.br', 'publicado(a)', 0),
(748, '', ' franciscojaques@petrobras.com.br', 'publicado(a)', 0),
(749, '', ' carlos.j.aguiar@petrobras.com.br', 'publicado(a)', 0),
(750, '', ' panfili@petrobras.com.br', 'publicado(a)', 0),
(751, '', ' rigel@petrobras.com.br', 'publicado(a)', 0),
(752, '', ' marcoajpereira@petrobras.com.br', 'publicado(a)', 0),
(753, '', ' helioweber@petrobras.com.br', 'publicado(a)', 0),
(754, '', ' flaviorony@petrobras.com.br', 'publicado(a)', 0),
(755, '', ' coelho@petrobras.com.br', 'publicado(a)', 0),
(756, '', ' domingosb@petrobras.com.br', 'publicado(a)', 0),
(757, '', ' avelino@petrobras.com.br', 'publicado(a)', 0),
(758, '', ' chiconet@petrobras.com.br', 'publicado(a)', 0),
(759, '', ' bjsantos@petrobras.com.br', 'publicado(a)', 0),
(760, '', ' aanatalino@petrobras.com.br', 'publicado(a)', 0),
(761, '', ' dabryw@petrobras.com.br', 'publicado(a)', 0),
(762, '', ' nelsondias@petrobras.com.br', 'publicado(a)', 0),
(763, '', ' joblima@petrobras.com.br', 'publicado(a)', 0),
(764, '', ' joseedilson@petrobras.com.br', 'publicado(a)', 0),
(765, '', ' ricardo.azevedo@petrobras.com.br', 'publicado(a)', 0),
(766, '', ' mauroalfradique@petrobras.com.br', 'publicado(a)', 0),
(767, '', ' henriquemaia@petrobras.com.br', 'publicado(a)', 0),
(768, '', ' antonioh.rosa@petrobras.com.br', 'publicado(a)', 0),
(769, '', ' antonioh.rosa@petrobras.com.br', 'publicado(a)', 0),
(770, '', ' liborio@petrobras.com.br', 'publicado(a)', 0),
(771, '', ' rnj@petrobras.com.br', 'publicado(a)', 0),
(772, '', ' luispar@petrobras.com.br', 'publicado(a)', 0),
(773, '', ' sergiobiah@petrobras.com.br', 'publicado(a)', 0),
(774, '', ' fernandomello@petrobras.com.br', 'publicado(a)', 0),
(775, '', ' wagnerggp@petrobras.com.br', 'publicado(a)', 0),
(776, '', ' jotalc@petrobras.com.br', 'publicado(a)', 0),
(777, '', ' marcelo.m@petrobras.com.br', 'publicado(a)', 0),
(778, '', ' raphaelmarchon@petrobras.com.br', 'publicado(a)', 0),
(779, '', ' paulorotta@petrobras.com.br', 'publicado(a)', 0),
(780, '', ' bittencourtfilho@petrobras.com.br', 'publicado(a)', 0),
(781, '', ' ricci@petrobras.com.br', 'publicado(a)', 0),
(782, '', ' rejotha@petrobras.com.br', 'publicado(a)', 0),
(783, '', ' ricardo_gomes@petrobras.com.br', 'publicado(a)', 0),
(784, '', ' hbrandt@petrobras.com.br', 'publicado(a)', 0),
(785, '', ' cegs@petrobras.com.br', 'publicado(a)', 0),
(786, '', ' carlos77@petrobras.com.br', 'publicado(a)', 0),
(787, '', ' f.albuquerque@petrobras.com.br', 'publicado(a)', 0),
(788, '', ' joseprisco@petrobras.com.br', 'publicado(a)', 0),
(789, '', ' cleitonrat@petrobras.com.br', 'publicado(a)', 0),
(790, '', ' romanobr@petrobras.com.br', 'publicado(a)', 0),
(791, '', ' jason.br@petrobras.com.br', 'publicado(a)', 0),
(792, '', ' aclaudioc@petrobras.com.br', 'publicado(a)', 0),
(793, '', ' ana_guidini@petrobras.com.br', 'publicado(a)', 0),
(794, '', ' rodrigofreitas@petrobras.com.br', 'publicado(a)', 0),
(795, '', ' flaviosmiranda@petrobras.com.br', 'publicado(a)', 0),
(796, '', ' cosmedamiao@petrobras.com.br', 'publicado(a)', 0),
(797, '', ' iltonsias@petrobras.com.br', 'publicado(a)', 0),
(798, '', ' philipecanha@petrobras.com.br', 'publicado(a)', 0),
(799, '', ' cadha@petrobras.com.br', 'publicado(a)', 0),
(800, '', ' mresende@petrobras.com.br', 'publicado(a)', 0),
(801, '', ' mbesse@petrobras.com.br', 'publicado(a)', 0),
(802, '', ' andersonjms@petrobras.com.br', 'publicado(a)', 0),
(803, '', ' thiago_henriques@petrobras.com.br', 'publicado(a)', 0),
(804, '', ' guilherme.fonseca@petrobras.com.br', 'publicado(a)', 0),
(805, '', ' cunhaviana@petrobras.com.br', 'publicado(a)', 0),
(806, '', ' leandrocarvalho@petrobras.com.br', 'publicado(a)', 0),
(807, '', ' lssjunior@petrobras.com.br', 'publicado(a)', 0),
(808, '', ' jrcsilva@petrobras.com.br', 'publicado(a)', 0),
(809, '', ' mdferreira@petrobras.com.br', 'publicado(a)', 0),
(810, '', ' orisvaldojunior@petrobras.com.br', 'publicado(a)', 0),
(811, '', ' eberjtp@petrobras.com.br', 'publicado(a)', 0),
(812, '', ' laiz.almeida@petrobras.com.br', 'publicado(a)', 0),
(813, '', ' alessandromec@petrobras.com.br', 'publicado(a)', 0),
(814, '', ' alleylp@petrobras.com.br', 'publicado(a)', 0),
(815, '', ' luisclaudiotec@petrobras.com.br', 'publicado(a)', 0),
(816, '', ' marciocsouza100@petrobras.com.br', 'publicado(a)', 0),
(817, '', ' thiagoscherrer@petrobras.com.br', 'publicado(a)', 0),
(818, '', ' thiago.gomes@petrobras.com.br', 'publicado(a)', 0),
(819, '', ' m.dias@petrobras.com.br', 'publicado(a)', 0),
(820, '', ' sidrodrigues@petrobras.com.br', 'publicado(a)', 0),
(821, '', ' cmscandinho@petrobras.com.br', 'publicado(a)', 0),
(822, '', ' leandrovasconcellos@petrobras.com.br', 'publicado(a)', 0),
(823, '', ' leandro.mota@petrobras.com.br', 'publicado(a)', 0),
(824, '', ' ofp_69@hotmail.com', 'publicado(a)', 0),
(825, '', ' mauriciofc@petrobras.com.br', 'publicado(a)', 0),
(826, '', ' tadeujose@petrobras.com.br', 'publicado(a)', 0),
(827, '', ' edcoelho@petrobras.com.br', 'publicado(a)', 0),
(828, '', ' sergio.silva@petrobras.com.br', 'publicado(a)', 0),
(829, '', ' crsrodrigues@petrobras.com.br', 'publicado(a)', 0),
(830, '', ' amilton@petrobras.com.br', 'publicado(a)', 0),
(831, '', ' manoelpereira@petrobras.com.br', 'publicado(a)', 0),
(832, '', ' edsilva@petrobras.com.br', 'publicado(a)', 0),
(833, '', ' jcmonteiro@petrobras.com.br', 'publicado(a)', 0),
(834, '', ' edsilva@petrobras.com.br', 'publicado(a)', 0),
(835, '', ' efd@petrobras.com.br', 'publicado(a)', 0),
(836, '', ' mroberto@petrobras.com.br', 'publicado(a)', 0),
(837, '', ' marcosfirmino@petrobras.com.br', 'publicado(a)', 0),
(838, '', ' rodolfofb@petrobras.com.br', 'publicado(a)', 0),
(839, '', ' joaocarlos@petrobras.com.br', 'publicado(a)', 0),
(840, '', ' pvinicio@petrobras.com.br', 'publicado(a)', 0),
(841, '', ' tvxavier@petrobras.com.br', 'publicado(a)', 0),
(842, '', ' marvargas@petrobras.com.br', 'publicado(a)', 0),
(843, '', ' asmeireles@petrobras.com.br', 'publicado(a)', 0),
(844, '', ' mrma@petrobras.com.br', 'publicado(a)', 0),
(845, '', ' jmauroribeiro@petrobras.com.br', 'publicado(a)', 0),
(846, '', ' wilsonrangel@petrobras.com.br', 'publicado(a)', 0),
(847, '', ' m_nogueira@petrobras.com.br', 'publicado(a)', 0),
(848, '', ' francisco.oliveira@petrobras.com.br', 'publicado(a)', 0),
(849, '', ' jamachado@petrobras.com.br', 'publicado(a)', 0),
(850, '', ' vicenteramos@petrobras.com.br', 'publicado(a)', 0),
(851, '', ' williamjunger@petrobras.com.br', 'publicado(a)', 0),
(852, '', ' rosedepaula@petrobras.com.br', 'publicado(a)', 0),
(853, '', ' veronicaavila@petrobras.com.br', 'publicado(a)', 0),
(854, '', ' paulacamacho@petrobras.com.br', 'publicado(a)', 0),
(855, '', ' apsouza@petrobras.com.br', 'publicado(a)', 0),
(856, '', ' joseluizp@petrobras.com.br', 'publicado(a)', 0),
(857, '', ' arlington@petrobras.com.br', 'publicado(a)', 0),
(858, '', ' dagelo@petrobras.com.br', 'publicado(a)', 0),
(859, '', ' luiznailson@petrobras.com.br', 'publicado(a)', 0),
(860, '', ' magrassi@petrobras.com.br', 'publicado(a)', 0),
(861, '', ' megarcia@petrobras.com.br', 'publicado(a)', 0),
(862, '', ' thiagoa@petrobras.com.br', 'publicado(a)', 0),
(863, '', ' clausmar@petrobras.com.br', 'publicado(a)', 0),
(864, '', ' zebra@petrobras.com.br', 'publicado(a)', 0),
(865, '', ' nelvan@petrobras.com.br', 'publicado(a)', 0),
(866, '', ' aao@petrobras.com.br', 'publicado(a)', 0),
(867, '', ' jobdantas@petrobras.com.br', 'publicado(a)', 0),
(868, '', ' csaraujo@petrobras.com.br', 'publicado(a)', 0),
(869, '', ' jrobson@petrobras.com.br', 'publicado(a)', 0),
(870, '', ' genesis@petrobras.com.br', 'publicado(a)', 0),
(871, '', ' tarcisoa@petrobras.com.br', 'publicado(a)', 0),
(872, '', ' albertopr@petrobras.com.br', 'publicado(a)', 0),
(873, '', ' ans@petrobras.com.br', 'publicado(a)', 0),
(874, '', ' cornelius@petrobras.com.br', 'publicado(a)', 0),
(875, '', ' geraldogmnk@petrobras.com.br', 'publicado(a)', 0),
(876, '', ' walterl@petrobras.com.br', 'publicado(a)', 0),
(877, '', ' rlevi@petrobras.com.br', 'publicado(a)', 0),
(878, '', ' cajunior@petrobras.com.br', 'publicado(a)', 0),
(879, '', ' ricardoteixeira@petrobras.com.br', 'publicado(a)', 0),
(880, '', ' renata.souza@petrobras.com.br', 'publicado(a)', 0),
(881, '', ' havila@petrobras.com.br', 'publicado(a)', 0),
(882, '', ' humbertohc@petrobras.com.br', 'publicado(a)', 0),
(883, '', ' denilsonrodrigues@petrobras.com.br', 'publicado(a)', 0),
(884, '', ' tiagoleviski@petrobras.com.br', 'publicado(a)', 0),
(885, '', ' marceloalves@petrobras.com.br', 'publicado(a)', 0),
(886, '', ' e_tavares2003@petrobras.com.br', 'publicado(a)', 0),
(887, '', ' leonardo.tbarbosa@petrobras.com.br', 'publicado(a)', 0),
(888, '', ' danilonascimento@petrobras.com.br', 'publicado(a)', 0),
(889, '', ' daviddias@petrobras.com.br', 'publicado(a)', 0),
(890, '', ' alancr@petrobras.com.br', 'publicado(a)', 0),
(891, '', ' marcio.mendes@petrobras.com.br', 'publicado(a)', 0),
(892, '', ' ofp_69@hotmail.com', 'publicado(a)', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `email_fale_conosco`
--

CREATE TABLE `email_fale_conosco` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `email_fale_conosco`
--

INSERT INTO `email_fale_conosco` (`id`, `email`) VALUES
(1, 'contato@globalnetsis.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `enquete`
--

CREATE TABLE `enquete` (
  `id` int(11) NOT NULL auto_increment,
  `opcao01` varchar(255) collate utf8_unicode_ci NOT NULL,
  `opcao01_n` int(11) NOT NULL,
  `opcao02` varchar(255) collate utf8_unicode_ci NOT NULL,
  `opcao02_n` int(11) NOT NULL,
  `opcao03` varchar(255) collate utf8_unicode_ci NOT NULL,
  `opcao03_n` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `pergunta` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `enquete`
--

INSERT INTO `enquete` (`id`, `opcao01`, `opcao01_n`, `opcao02`, `opcao02_n`, `opcao03`, `opcao03_n`, `total`, `pergunta`) VALUES
(15, 'Cachorro', 0, 'Gato', 0, 'Coelho', 0, 0, 'Qual o seu animal de estimaÃ§Ã£o favorito?'),
(14, 'Cachorro', 4, 'Gato', 2, 'Coelho', 0, 6, 'Qual o seu animal de estimaÃ§Ã£o favorito?'),
(16, 'Higiene', 0, 'Bom atendimento', 0, 'EstÃ©tica', 0, 0, 'Quais serviÃ§os vocÃª considera de primeira necessidade na hospedagem?');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fabricantes`
--

CREATE TABLE `fabricantes` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `url` text collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `fabricantes`
--

INSERT INTO `fabricantes` (`id`, `nome`, `url`, `descricao`, `imagem`, `thumb`, `data`, `hora`, `status`) VALUES
(1, 'Hering', 'hering.com.br', '<p>Lorem.</p>', '', '', '2012-03-28', '10:07:00', 'publicado(a)'),
(2, 'Bosch', 'bosch.com.br', '', '', '', '2012-03-31', '00:00:00', 'publicado(a)'),
(3, 'Space', 'space.com.br', '', '', '', '2012-03-27', '00:00:00', 'publicado(a)'),
(4, 'Shell', 'shell.com.br', '<p>Distribuidor de combust&iacute;vel.</p>', 'fotos/23620606_03042012.jpg', 'fotos/23620606_03042012_thumb.jpg', '2012-04-03', '10:17:16', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos_galeria`
--

CREATE TABLE `fotos_galeria` (
  `id` int(11) NOT NULL auto_increment,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `id_galeria` int(11) NOT NULL,
  `nome_galeria` varchar(255) collate utf8_unicode_ci NOT NULL,
  `legenda` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=123 ;

--
-- Extraindo dados da tabela `fotos_galeria`
--

INSERT INTO `fotos_galeria` (`id`, `imagem`, `thumb`, `id_galeria`, `nome_galeria`, `legenda`) VALUES
(119, 'fotos/8360_04042012.jpg', 'fotos/8360_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(118, 'fotos/2476_04042012.jpg', 'fotos/2476_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(117, 'fotos/1074_04042012.jpg', 'fotos/1074_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(116, 'fotos/1335_04042012.jpg', 'fotos/1335_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(108, 'fotos/8631_04042012.jpg', 'fotos/8631_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(115, 'fotos/4953_04042012.jpg', 'fotos/4953_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(114, 'fotos/4309_04042012.jpg', 'fotos/4309_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(113, 'fotos/5821_04042012.jpg', 'fotos/5821_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(111, 'fotos/6683_04042012.jpg', 'fotos/6683_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(112, 'fotos/9250_04042012.jpg', 'fotos/9250_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(110, 'fotos/9949_04042012.jpg', 'fotos/9949_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(109, 'fotos/236_04042012.jpg', 'fotos/236_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(107, 'fotos/1362_04042012.jpg', 'fotos/1362_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(106, 'fotos/8461_04042012.jpg', 'fotos/8461_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(105, 'fotos/77987671_24052012.jpg', 'fotos/77987671_24052012_thumb.jpg', 17, 'Festa da Escola', ''),
(122, 'fotos/4763_04042012.jpg', 'fotos/4763_04042012_thumb.jpg', 17, 'Festa da Escola', ''),
(121, 'fotos/5681_04042012.jpg', 'fotos/5681_04042012_thumb.jpg', 17, 'Festa da Escola', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `autor` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `galeria`
--

INSERT INTO `galeria` (`id`, `nome`, `descricao`, `data`, `hora`, `autor`, `status`) VALUES
(17, 'Festa da Escola', '<p>Lorem Ipsum.</p>', '2012-04-04', '14:29:19', 'AnÃ´nimo', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `url` text collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `links`
--

INSERT INTO `links` (`id`, `nome`, `descricao`, `url`, `imagem`, `thumb`, `status`, `data`, `hora`) VALUES
(3, 'Plano de SaÃºde Ases', '<p>Plano.</p>', 'ases.com.br', 'fotos/2967834527032012.jpg', 'fotos/pequena_2967834527032012.jpg', 'publicado(a)', '2012-05-24', '16:09:05'),
(4, 'Unimed', '<p>Plano de sa&uacute;de.</p>', 'unimed.com.br', 'fotos/56613159_24052012.jpg', 'fotos/56613159_24052012_thumb.jpg', 'publicado(a)', '2012-05-22', '14:32:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `menu`
--

INSERT INTO `menu` (`id`, `nome`, `status`) VALUES
(7, 'email_fale_conosco', 'publicado(a)'),
(6, 'noticias', 'publicado(a)'),
(8, 'links', 'publicado(a)'),
(9, 'usuarios', 'publicado(a)'),
(10, 'enquete', 'publicado(a)'),
(11, 'menu', 'publicado(a)'),
(12, 'categorias', 'publicado(a)'),
(13, 'fabricantes', 'publicado(a)'),
(14, 'galeria', 'publicado(a)'),
(15, 'newsletter', 'publicado(a)'),
(16, 'produtos', 'publicado(a)'),
(17, 'solicitacoes', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menu_usuarios`
--

CREATE TABLE `menu_usuarios` (
  `id` int(11) NOT NULL auto_increment,
  `id_usuario` int(11) NOT NULL,
  `login_usuario` varchar(255) collate utf8_unicode_ci NOT NULL,
  `id_menu` int(11) NOT NULL,
  `nome_menu` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `menu_usuarios`
--

INSERT INTO `menu_usuarios` (`id`, `id_usuario`, `login_usuario`, `id_menu`, `nome_menu`) VALUES
(6, 11, 'felipe', 7, 'categorias'),
(7, 11, 'felipe', 10, 'clientes'),
(8, 11, 'felipe', 8, 'links');

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `autor` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `nome`, `descricao`, `imagem`, `thumb`, `data`, `hora`, `autor`, `status`) VALUES
(10, 'Cielo conquista tri e Murer leva o bi', '<p><span>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</span></p>', 'fotos/1705017126032012.jpg', 'fotos/pequena_1705017126032012.jpg', '2012-03-24', '03:58:01', 'AnÃ´nimo', 'publicado(a)'),
(11, 'PARA VOCÃŠ PENSAR', '<p><span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span></p>', 'fotos/1265604943.jpg', 'fotos/pequena_1265604943.jpg', '2012-03-23', '05:04:57', 'AnÃ´nimo', 'publicado(a)'),
(12, 'SER UMA PESSOA AGRADECIDA', '<p><span>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span></p>', 'fotos/990214709.jpg', 'fotos/pequena_990214709.jpg', '2012-03-23', '02:51:23', 'AnÃ´nimo', 'publicado(a)'),
(13, 'CARBOXITERAPIA', '<p>Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum.</p>\r\n<p>Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum.</p>', 'fotos/8683776823032012.jpg', 'fotos/pequena_86837768.jpg', '2012-03-23', '05:52:20', 'AnÃ´nimo', 'publicado(a)'),
(14, 'Maria Joaquina', '<p>Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum.</p>\r\n<p>Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum&nbsp;Lorem Ipsum Lorem Ipsum.</p>', 'fotos/84814453_12042012.jpg', 'fotos/84814453_12042012_thumb.jpg', '2012-03-23', '00:22:06', 'AnÃ´nimo2', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `preco` decimal(10,2) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  `id_fabricante` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `descricao`, `imagem`, `thumb`, `preco`, `data`, `hora`, `status`, `id_fabricante`, `id_categoria`) VALUES
(2, 'Protetor de carter', '<p>Ipsum Lorem Ipsum Lorem Ipsum.</p>', 'fotos/440807473.jpg', 'fotos/pequena_440807473.jpg', 52.05, '2012-03-22', '10:43:00', 'publicado(a)', 1, 2),
(7, 'Aeromisth', '<p>Jaded.</p>', 'fotos/1667275303.jpg', 'fotos/pequena_1667275303.jpg', 51.00, '0000-00-00', '00:00:00', 'publicado(a)', 3, 3),
(8, 'Lupo', '<p>Meia Ipsum Lorem meia.</p>', 'fotos/371688129.jpg', 'fotos/pequena_371688129.jpg', 15.00, '0000-00-00', '00:00:00', 'publicado(a)', 2, 2),
(9, 'Mesa de frios', '<p>Lorem.</p>', 'fotos/920866493.jpg', 'fotos/pequena_920866493.jpg', 280.00, '0000-00-00', '00:00:00', 'publicado(a)', 2, 4),
(15, 'Produto 22', '<p>Lorem</p>', 'fotos/120729981_03042012.jpg', 'fotos/120729981_03042012_thumb.jpg', 50.00, '2012-04-03', '10:57:01', 'publicado(a)', 1, 1),
(13, 'Celular', '<p>Lorem Ipsum Lorem.</p>', 'fotos/1453337201.jpg', 'fotos/pequena_1453337201.jpg', 500.00, '0000-00-00', '00:00:00', 'publicado(a)', 1, 1),
(11, 'Impressora', '<p>Lorem Ipsum Lorem</p>', 'fotos/762200979.jpg', 'fotos/pequena_762200979.jpg', 290.00, '0000-00-00', '00:00:00', 'publicado(a)', 2, 3),
(12, 'Bolsa Tirret', '<p>Ipsum Lorem.</p>', 'fotos/1291696351.jpg', 'fotos/pequena_1291696351.jpg', 120.00, '0000-00-00', '00:00:00', 'publicado(a)', 1, 1),
(14, 'Produto 011', '<p>Lorem.</p>', 'fotos/932424529.jpg', 'fotos/pequena_932424529.jpg', 125.50, '2012-03-22', '10:39:45', 'bloqueado(a)', 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes`
--

CREATE TABLE `solicitacoes` (
  `id` int(11) NOT NULL auto_increment,
  `texto` text collate utf8_unicode_ci NOT NULL,
  `codigo` int(11) NOT NULL,
  `solicitante` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email_solicitante` varchar(255) collate utf8_unicode_ci NOT NULL,
  `prioridade` varchar(255) collate utf8_unicode_ci NOT NULL,
  `data_abertura` date NOT NULL,
  `hora_abertura` time NOT NULL,
  `data_previsao` date NOT NULL,
  `hora_previsao` time NOT NULL,
  `data_conclusao` date NOT NULL,
  `hora_conclusao` time NOT NULL,
  `dominio_cliente` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `solicitacoes`
--

INSERT INTO `solicitacoes` (`id`, `texto`, `codigo`, `solicitante`, `email_solicitante`, `prioridade`, `data_abertura`, `hora_abertura`, `data_previsao`, `hora_previsao`, `data_conclusao`, `hora_conclusao`, `dominio_cliente`, `status`) VALUES
(3, 'Lorem Ipsum.<br />\r\nIpsum.', 1333133985, 'Jorge LautÃ©rio', 'jorgelauterio@ig.com.br', 'Alta', '2012-03-30', '15:59:45', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'www.santuarioperpetuosocorro.org.br', 'Aberta'),
(4, 'Lorem Ipsum.<br />\r\nIpsum.', 1333137126, 'Jorge LautÃ©rio', 'jorgelauterio@ig.com.br', 'Alta', '2012-03-30', '16:52:06', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'www.santuarioperpetuosocorro.org.br', 'Aberta');

-- --------------------------------------------------------

--
-- Estrutura da tabela `texto_solicitacoes`
--

CREATE TABLE `texto_solicitacoes` (
  `id` int(11) NOT NULL auto_increment,
  `texto` text collate utf8_unicode_ci NOT NULL,
  `id_solicitacao` int(11) NOT NULL,
  `responsavel` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `texto_solicitacoes`
--

INSERT INTO `texto_solicitacoes` (`id`, `texto`, `id_solicitacao`, `responsavel`) VALUES
(3, 'Lorem Ipsum.<br />\r\nIpsum.', 3, 'Jorge LautÃ©rio'),
(4, 'Lorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.\r\nLorem Ipsum.<br />\r\nIpsum.', 4, 'Jorge LautÃ©rio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci NOT NULL,
  `login` varchar(255) collate utf8_unicode_ci NOT NULL,
  `senha` varchar(255) collate utf8_unicode_ci NOT NULL,
  `imagem` text collate utf8_unicode_ci NOT NULL,
  `thumb` text collate utf8_unicode_ci NOT NULL,
  `permissao` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `email`, `login`, `senha`, `imagem`, `thumb`, `permissao`, `status`) VALUES
(14, 'Globalnetsis', 'contato@globalnetsis.com.br', 'global', 'ssuser03', '', '', 'Administrador', 'publicado(a)'),
(13, 'Administador', 'admin@globalnetsis.com.br', 'admin', 'admin', '', '', 'Administrador', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL auto_increment,
  `url` text collate utf8_unicode_ci NOT NULL,
  `nome` varchar(255) collate utf8_unicode_ci NOT NULL,
  `descricao` text collate utf8_unicode_ci NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `autor` varchar(255) collate utf8_unicode_ci NOT NULL,
  `status` varchar(255) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `videos`
--

INSERT INTO `videos` (`id`, `url`, `nome`, `descricao`, `data`, `hora`, `autor`, `status`) VALUES
(9, 'http://www.youtube.com/v/G7b-_YcACuQ&feature=g-vrec&context=G2b459bdRVAAAAAAAABQ', 'Teste', '<p>Ipsum.</p>', '2012-03-31', '11:15:45', 'AnÃ´nimo', 'publicado(a)'),
(7, 'http://www.youtube.com/v/7YvdeuKadgw&feature=g-vrec&context=G2c4c169RVAAAAAAAAAQ', 'VÃ­deo de Andy Timmons', '<p>Lorem Ipsum.</p>', '2012-03-28', '10:39:28', 'AnÃ´nimo', 'publicado(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `visitas`
--

CREATE TABLE `visitas` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `data` date NOT NULL,
  `uniques` int(10) unsigned NOT NULL default '0',
  `pageviews` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `data` (`data`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Extraindo dados da tabela `visitas`
--

INSERT INTO `visitas` (`id`, `data`, `uniques`, `pageviews`) VALUES
(1, '2012-05-02', 1, 180),
(2, '2012-05-03', 1, 12),
(3, '2012-05-04', 13, 17),
(4, '2012-05-15', 1, 52),
(5, '2012-05-16', 45, 79),
(6, '2012-05-18', 29, 29),
(7, '2012-05-22', 1, 7),
(8, '2012-05-24', 3, 31),
(9, '2012-06-08', 1, 1),
(10, '2012-06-12', 1, 1),
(11, '2012-06-14', 1, 1),
(12, '2012-06-15', 2, 2),
(13, '2012-06-20', 1, 1),
(14, '2012-06-22', 1, 1),
(15, '2012-06-25', 1, 1),
(16, '2012-06-26', 1, 1),
(17, '2012-06-28', 1, 1),
(18, '2012-06-29', 2, 3),
(19, '2012-07-02', 1, 1),
(20, '2012-07-09', 1, 2),
(21, '2012-07-10', 1, 1),
(22, '2012-07-13', 1, 2),
(23, '2012-07-17', 1, 1),
(24, '2012-07-19', 1, 1),
(25, '2012-07-30', 1, 1),
(26, '2012-08-01', 1, 1),
(27, '2012-08-08', 2, 3),
(28, '2012-08-14', 1, 1),
(29, '2012-08-15', 2, 2),
(30, '2012-08-28', 1, 1),
(31, '2012-08-29', 1, 1),
(32, '2012-09-10', 1, 1),
(33, '2012-09-24', 1, 1),
(34, '2012-10-01', 1, 1),
(35, '2012-11-14', 1, 1),
(36, '2012-11-16', 1, 1),
(37, '2012-11-26', 1, 1),
(38, '2012-11-29', 1, 1),
(39, '2012-11-30', 1, 1),
(40, '2012-12-17', 1, 1),
(41, '2012-12-27', 1, 1),
(42, '2012-12-28', 2, 2),
(43, '2013-01-03', 1, 1),
(44, '2013-01-09', 1, 1),
(45, '2013-01-10', 1, 1),
(46, '2013-01-23', 1, 2),
(47, '2013-01-24', 1, 1),
(48, '2013-03-11', 1, 1),
(49, '2013-03-12', 1, 1),
(50, '2013-03-20', 2, 2);
