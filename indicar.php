<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RR Engenharia</title>
<META NAME="description" CONTENT="RR Engenharia">
<META NAME="keywords" CONTENT="RR Engenharia, RR, RR Campos, Campos, RR Engenharia Campos dos Goytacazes, Engenharia, Engenharia Campos, Construção, Construção Campos, Construção RJ, RJ">
<style type="text/css" media="screen">@import "style.css"; @import url(http://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic);</style>
<script language="javascript" src="js/lib/formata.js"></script>
<script language="javascript" src="js/lib/validacao_indicar.js"></script>
</head>

<body>

 <div id="bg_top"></div>
 
   <div id="container">
     <?php include 'header.php'; ?> 
        <div id="content">
          <div class="cabecalho">INDIQUE PARA UM AMIGO</div>
           
            <p class="path padding_path">//<a href="index.php?menu=index" class="path">HOME</a>/<b>INDIQUE PARA UM AMIGO</b></p>
              <hr class="hr">
                
               <p class="path padding_path">Indique o site da <b>RR Engenharia</b> para um amigo.</p>
                
                <div class="padding_contato texto_contato"> 
                   <form action="indicar_enviar.php" method="post" name="dados" onsubmit="return enviardados();">
                      
                      <label for="seu_nome">seu nome</label>
			            <input type="text" name="seu_nome" id="seu_nome" size="40" class="input_contato"/>
			          
                      <label for="seu_email">seu e-mail</label>
			        	<input type="text" name="seu_email" id="seu_email" size="40" class="input_contato" />
                      
                      <label for="nome_amigo">nome do amigo</label>
			         	<input type="text" name="nome_amigo" id="nome_amigo" size="40" class="input_contato" />
			          
                      <label for="email_amigo">e-mail do amigo</label>
			        	<input type="text" name="email_amigo" id="email_amigo" size="40" class="input_contato" />
                  		
                      	<br />
                  		<br />
                  		<!--anti spam--><input type="text" name="vazio" value="" style="display:none" />
                      
                      <label for="email">mensagem</label>
			      		<textarea rows="5" cols="32" name="mensagem" class="input_contato"></textarea>
			      	  
                        <p>&nbsp;</p>
                  		  <div align="right">
                    		<input type="submit" value=" &nbsp;enviar&nbsp; " class="button" />
                  		  </div>
                 	</form>   
                 </div>  
                
         <p>&nbsp;</p>     
			  
			  <?php include "rodape.php";?>
          </div><!--content-->  
    </div><!--container-->
  
</body>
</html>