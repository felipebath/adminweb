<?php
session_start();
if (!isset($_SESSION['login']) || !isset($_SESSION['senha'])){
header("Location: ../../login.php?erro=1");
exit;
}
if (isset($_GET['logoff'])){
session_unset();
session_destroy();
header("Location: ../../login.php");
}
?>