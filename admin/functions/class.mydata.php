<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?
################################################################################
## CLASS CRIADA POR: Joo Hueder da Silva                                     ##
## DATA............: 14/05/2006                                               ##
## E-MAIL..........: joaohueder@gmail.com                                     ##
## MSN.............: joaohueder@hotmail.com                                   ##
## SKYPE...........: joaohueder                                               ##
## VERSO..........: 1.0                                                      ##
################################################################################

class myData
      {
      //Funo para mostrar a data por extenso, inclusive com saudao
      function myData()
            {
            $SAUDACAO         = array('','Bom dia,','Boa tarde,','Boa noite,');
            $DIA_EXTENSSO     = array('','Segunda-feira,','Terça-feira,','Quarta-feira,','Quinta-feira,','Sexta-feira,','Sábado,','Domingo,');
            $MES_EXTENSSO     = array('','janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro');

            if (date('H') <= '12') { $saudacao = $SAUDACAO[1]; } else if (date('H') <= '18') { $saudacao = $SAUDACAO[2]; } else if (date('H') <= '24') { $saudacao = $SAUDACAO[3]; }

            return $saudacao . " hoje é  " . $DIA_EXTENSSO[date('w')] . " " . date('d') . " de " . $MES_EXTENSSO[date('n')] . " de " . date('Y') . '<br><br>';
            }
      }


$funcoes = new myData();
echo $funcoes->myData();
?>