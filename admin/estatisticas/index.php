<?php
include "../verificacao_de_login.php";

/**
 * @author Thiago Belem <contato@thiagobelem.net>
 * @link http://thiagobelem.net/
 *
 * @version 1.0
 */

 //  Configurações do Script
 // ==============================
 $_CV['registraAuto'] = true;       // Registra as visitas automaticamente?
 $_CV['conectaMySQL'] = true;       // Abre uma conexão com o servidor MySQL?
 $_CV['iniciaSessao'] = true;       // Inicia a sessão com um session_start()?
 $_CV['tabela'] = 'visitas';        // Nome da tabela onde os dados são salvos
   include '../../config/config.php';
 
 // Verifica se precisa iniciar a sessão
 if ($_CV['iniciaSessao'] == true) {
    session_start();
 }

 function registraVisita() {
    global $_CV;

    $sql = "SELECT COUNT(*) FROM `".$_CV['tabela']."` WHERE `data` = CURDATE()";
    $query = mysql_query($sql);
    $resultado = mysql_fetch_row($query);

    // Verifica se é uma visita (do visitante)
    $nova = (!isset($_SESSION['ContadorVisitas'])) ? true : false;

    // Verifica se já existe registro para o dia
    if ($resultado[0] == 0) {
        //$sql = "INSERT INTO `".$_CV['tabela']."` VALUES (NULL, CURDATE(), 1, 1)";
    } else {
        if ($nova == true) {
            //$sql = "UPDATE `".$_CV['tabela']."` SET `uniques` = (`uniques` + 1), `pageviews` = (`pageviews` + 1) WHERE `data` = CURDATE()";
        } else {
            //$sql = "UPDATE `".$_CV['tabela']."` SET `pageviews` = (`pageviews` + 1) WHERE `data` = CURDATE()";
        }
    }
    // Registra a visita
    mysql_query($sql);

    // Cria uma variavel na sessão
    $_SESSION['ContadorVisitas'] = md5(time());
 }

/**
 * Função que retorna o total de visitas
 *
 * @param string $tipo - O tipo de visitas a se pegar: (uniques|pageviews)
 * @param string $periodo - O período das visitas: (hoje|mes|ano)
 *
 * @return int - Total de visitas do tipo no período
 */
 function pegaVisitas($tipo = 'uniques', $periodo = 'hoje') {
    global $_CV;

    switch($tipo) {
        default:
        case 'uniques':
            $campo = 'uniques';
            break;
        case 'pageviews':
            $campo = 'pageviews';
            break;
    }

    switch($periodo) {
        default:
        case 'hoje':
            $busca = "`data` = CURDATE()";
            break;
        case 'mes':
            $busca = "`data` BETWEEN DATE_FORMAT(CURDATE(), '%Y-%m-01') AND LAST_DAY(CURDATE())";
            break;
        case 'ano':
            $busca = "`data` BETWEEN DATE_FORMAT(CURDATE(), '%Y-01-01') AND DATE_FORMAT(CURDATE(), '%Y-12-31')";
            break;
    }

    // Faz a consulta no MySQL em função dos argumentos
    $sql = "SELECT SUM(`".$campo."`) FROM `".$_CV['tabela']."` WHERE ".$busca;
    $query = mysql_query($sql);
    $resultado = mysql_fetch_row($query);

    // Retorna o valor encontrado ou zero
    return (!empty($resultado)) ? (int)$resultado[0] : 0;
 }

 if ($_CV['registraAuto'] == true) { registraVisita(); }
 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Painel de Controle</title>
<script type="text/javascript" src="../js/confirmar_exclusao.js"></script>
<script type="text/javascript" src="../js/simpletabs_1.3.js"></script>
<link href="../css/painel.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="screen">
@import "../css/style.css";
@import "../css/simpletabs.css";
</style>



</head>
<body>
<div id="container">

  <h1>Estatísticas do Site</h1>
  
  
<?php include "../tinymce_pt/include_editor/editor.php";?>
  
  
  
  
  
  <div align="center">
  <a href="../painel.php" class="menu">Principal</a>&nbsp;&nbsp;
  <a href="javascript:history.go(-1)" class="menu">Voltar</a>&nbsp;&nbsp;
  <a href="../painel.php?logoff" class="menu">Sair</a>
  </div>
  
  <hr /></hr>
  
  <div class="simpleTabs">
    <ul class="simpleTabsNavigation">
      <li><a href="#">Visualizar</a></li>
    </ul>
    
    <div class="simpleTabsContent">
      <div style="margin-top:10px;">
	   
       <table width="50%">
         
         <tr style="background-color:#F8F8FF;">
           <td valign="top" style="padding:5px;">Visitas do dia</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas(); ?></b></td>
         </tr>  
       
         <tr style="background-color:#FFE4E1;">
           <td valign="top" style="padding:5px;">Visitas do mês</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas('uniques', 'mes'); ?></b></td>
         </tr>  
         
         <tr style="background-color:#F0FFF0;">
           <td valign="top" style="padding:5px;">Visitas do ano</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas('uniques', 'ano'); ?></b></td>
         </tr>  
         
         <tr style="background-color:#FFE1FF;">
           <td valign="top" style="padding:5px;">Páginas visitadas no dia</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas('pageviews'); ?></b></td>
         </tr>  
         
         <tr style="background-color:#FFFAFA;">
           <td valign="top" style="padding:5px;">Páginas visitadas no mês</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas('pageviews', 'mes'); ?></b></td>
         </tr>  
         
         <tr style="background-color:#FFEFDB;">
           <td valign="top" style="padding:5px;">Páginas visitadas no ano</td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total = pegaVisitas('pageviews', 'ano'); ?></b></td>
         </tr>
         
         <?php
		   $sql = "SELECT SUM(uniques) AS uniques FROM visitas WHERE id != '5455454' ";
           $exec = mysql_query($sql);
             while ($rows = mysql_fetch_assoc($exec)) {
                  $total_visitas = $rows['uniques'];
               }
		 ?>
         
         <tr style="background-color:#E0FFFF;">
           <td valign="top" style="padding:5px;"><b>Total de visitas do site</b></td>
           <td valign="top" align="right" style="padding:5px;"><b><?php echo $total_visitas; ?></b></td>
         </tr>
       
      </table>    
       
         
      </div>
    </div>
    
   
  
  
  </div>
  <p>&nbsp;</p>
 <?php include "../rodape.php";?>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-55649-19");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>