<?php
session_start();
if (!isset($_SESSION['login']) || !isset($_SESSION['senha'])){
header("Location: ../login.php?erro=1");
exit;
}

if (isset($_GET['logoff'])){
session_unset();
session_destroy();
header("Location: ../login.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Painel de Controle</title>
<script type="text/javascript" src="../js/simpletabs_1.3.js"></script>
<script type="text/javascript" src="../mascaras/mascaras.js"></script>
<link href="../css/painel.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="screen">
@import "../css/style.css";
@import "../css/simpletabs.css";
</style>
<script language="JavaScript" >
function enviardados(){

if(document.dados.nome.value=="" || document.dados.nome.value.length < 2)
{
alert( "Preencha o campo NOME corretamente!" );
document.dados.nome.focus();
return false;
}

return true;
}
</script>
</head>
<body>
<div id="container">

  <h1>Painel de controle</h1>
  
  <div align="center">
  <a href="../painel.php" class="menu">Principal</a>&nbsp;&nbsp;
  <a href="javascript:history.go(-1)" class="menu">Voltar</a>&nbsp;&nbsp;
  <a href="../painel.php?logoff" class="menu">Sair</a>
  </div>
  
  <hr /></hr>
  
  <div class="simpleTabs">
    <ul class="simpleTabsNavigation">
      <li><a href="#"></a></li>
      <li><a href="#">Listar todos</a></li>
    </ul>
    
    
    <div class="simpleTabsContent"></div>
    
    
    
    
    
   <div class="simpleTabsContent">
      <div>
        <div>
          <div>
            <div>
            
        <table width="100%" border="1" cellpadding="1" cellspacing="1" class="texto">  
          <tr bgcolor="#E0DFE3"><div align="center"><b>SOLICITAÇÕES</b></div></tr>
            <tr>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Código</b></div></td>
            <td width="40%" bgcolor="#B2B4BF"><div align="center"><b>Descrição</b></div></td>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Aberta em</b></div></td>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Status</b></div></td>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Previsão</b></div></td>
            <td width="10%" bgcolor="#B2B4BF" class="texto"><div align="center"><b>ATENDER SOLICITAÇÃO</b></div></td>
            <td width="10%" bgcolor="#B2B4BF" class="texto"><div align="center"><b>ENCERRAR SOLICITAÇÃO</b></div></td>
            </tr>
         
         
        <?php
		include "../config/config.php";
		$busca=mysql_query("SELECT * FROM solicitacao ORDER BY data DESC");
           if(!mysql_num_rows($busca)){
              echo 'Nenhum dado cadastrado na base de dados.';
              }else{
                   while($ver=mysql_fetch_row($busca)){
                      $id=$ver[0]; 
                      $descricao=strip_tags(substr($ver[1], 0, 100)).'...';
					  $data=$ver[2];
                      $data = implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) ==                      0 ? "-" : "/", $data))); 
					  $horario=$ver[3];
					  $codigo=$ver[4];
					  $status=$ver[5]; 
					  
					  $data_previsao=$ver[9];
					  $data_previsao = implode(preg_match("~\/~", $data_previsao) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data_previsao) ==                      0 ? "-" : "/", $data_previsao))); 
					   
          ?>
         
         
         
         <tr>
            <td bgcolor="#E0DFE3" align="center"><?php echo $codigo ?></td>
            <td bgcolor="#E0DFE3" align="left" style="padding-left:5px;"><?php echo $descricao; ?></td>
            <td bgcolor="#E0DFE3" align="center"><?php echo $data.'<br>às&nbsp;'.$horario ?></td>
            
            <td bgcolor="#E0DFE3" align="center">
			  <?php if ($status=='pendente' || $status=='Pendente')
			           echo '<font color=#FF0000>'.$status.'</font>';
					     else
					   echo $status;  
					   ?>
                       </td>
            
            <td bgcolor="#E0DFE3" align="center"><?php echo $data_previsao ?></td>
                             
                 <td bgcolor="#E0DFE3">
                      <div align="center">
                       <img src="../imagens/editar.gif" border="0" /> 
                       <a href="atender_solicitacao.php?id=<?php echo $id?>" title="atender" class="texto">Atender Solicitação</a>
                        </div>
                         </td>
                 
                 <td bgcolor="#E0DFE3">
                      <div align="center">
                       <img src="../imagens/excluir.jpg" border="0" /> 
                       <a href="encerrar_solicitacao.php?id=<?php echo $id?>" title="encerrar" class="texto">Encerrar Solicitação</a>
                        </div>
                         </td>
            
           
         </tr>
         <?php }} ?>
      </table>
          
            
            </div>
          </div>
        </div>
      </div>
    </div>
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  </div>
  <p>&nbsp;</p>
            <center>Desenvolvido por 
              <a href="http://www.globalnetsis.com.br" title="acesse o website" target="_blank">Globalnetsis</a>
              - Todos os direitos reservados.
              </center>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-55649-19");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
