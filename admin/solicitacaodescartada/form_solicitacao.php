<?php
session_start();
if (!isset($_SESSION['login']) || !isset($_SESSION['senha'])){
header("Location: ../login.php?erro=1");
exit;
}

if (isset($_GET['logoff'])){
session_unset();
session_destroy();
header("Location: ../login.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Painel de Controle</title>
<script type="text/javascript" src="../js/simpletabs_1.3.js"></script>
<script type="text/javascript" src="../mascaras/mascaras.js"></script>
<link href="../css/painel.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="screen">
@import "../css/style.css";
@import "../css/simpletabs.css";
</style>
<script language="JavaScript" >
function enviardados(){

if( document.dados.email.value=="" || document.dados.email.value.indexOf('@')==-1 || document.dados.email.value.indexOf('.')==-1 )
{
alert( "Preencha o campo E-MAIL corretamente!" );
document.dados.email.focus();
return false;
}

return true;
}
</script>
</head>
<body>
<div id="container">

  <h1>Painel de controle</h1>
  
  <div align="center">
  <a href="../painel.php" class="menu">Principal</a>&nbsp;&nbsp;
  <a href="javascript:history.go(-1)" class="menu">Voltar</a>&nbsp;&nbsp;
  <a href="../painel.php?logoff" class="menu">Sair</a>
  </div>
  
  <hr /></hr>
  
  <div class="simpleTabs">
    <ul class="simpleTabsNavigation">
      <li><a href="#">Enviar solicitação</a></li>
      <li><a href="#">Listar todos</a></li>
    </ul>
    
    <div class="simpleTabsContent">
   <form action="enviar_solicitacao.php" method="post" enctype="multipart/form-data" name="dados" onsubmit="return enviardados();">
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descrição <textarea cols="80" rows="12" name="descricao"></textarea></p>
      
      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        E-mail <input type="text" name="email" size="40" />
      </p>
               
      <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prioridade <select name="prioridade">
          <option value="Baixa" selected="selected">Baixa</option>
            <option value="Media" >Média</option>
              <option value="Alta" >Alta</option>
                 </select>
                 </p>  
      
      <p>Solicitado por <input type="text" name="autor" size="50" /></p>
      <p><input type="submit" value=">> enviar solicitação" class="botao"/></p>
    </form>
    </div>
    
    
    
    
    
    
 
    
    
    
    
    
   <div class="simpleTabsContent">
      <div>
        <div>
          <div>
            <div>
            
        <table width="100%" border="1" cellpadding="1" cellspacing="1" class="texto">  
          <tr bgcolor="#E0DFE3"><div align="center"><b>SOLICITAÇÕES</b></div></tr>
            <tr>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Código</b></div></td>
            <td width="40%" bgcolor="#B2B4BF"><div align="center"><b>Interação</b></div></td>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Aberta em</b></div></td>
            <td width="10%" bgcolor="#B2B4BF"><div align="center"><b>Status</b></div></td>
            <td width="8%" bgcolor="#B2B4BF"><div align="center"><b>Previsão</b></div></td>
            <td width="12%" bgcolor="#B2B4BF" class="texto"><div align="center"><b>Solicitante</b></div></td>
            <td width="10%" bgcolor="#B2B4BF" class="texto"><div align="center"><b>VISUALIZAR</b></div></td>
            </tr>
         
         
        <?php
		include "../config/config.php";
		$busca=mysql_query("SELECT * FROM solicitacao ORDER BY data DESC");
           if(!mysql_num_rows($busca)){
              echo 'Nenhum dado cadastrado na base de dados.';
              }else{
                   while($ver=mysql_fetch_row($busca)){
                      $id=$ver[0]; 
                      $descricao=strip_tags(substr($ver[1], 0, 100)).'...';
					  $data=$ver[2];
                      $data = implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) ==                      0 ? "-" : "/", $data))); 
					  $horario=$ver[3];
					  $codigo=$ver[4];
					  $status=$ver[5];
					  $autor=$ver[6];
					  $data_previsao=$ver[9];
					  $data_previsao = implode(preg_match("~\/~", $data_previsao) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data_previsao) ==                      0 ? "-" : "/", $data_previsao)));                
          ?>
         
         
         
         <tr>
            <td bgcolor="#E0DFE3" align="center"><?php echo $codigo ?></td>
            <td bgcolor="#E0DFE3" align="left" style="padding-left:5px;"><?php echo $descricao; ?></td>
            <td bgcolor="#E0DFE3" align="center"><?php echo $data.'<br>às '.$horario ?></td>
            
            <td bgcolor="#E0DFE3" align="center">
			  <?php if ($status=='pendente' || $status=='Pendente')
			           echo '<font color=#FF0000>'.$status.'</font>';
					     else
					   echo $status;  
					   ?>
                       </td>
            
            
            <td bgcolor="#E0DFE3" align="center"><?php echo $data_previsao ?></td>
            <td bgcolor="#E0DFE3" align="center"><?php echo $autor?></a></td>
            
            <td bgcolor="#E0DFE3">
                      <div align="center">
                        <img src="../imagens/visualizar.jpg" border="0" /> 
                          <a href="detalhar_solicitacao.php?id=<?php echo $id?>" title="visualizar" class="texto">
                            Visualizar Solicitação
                          </a>
                      </div>
            </td>
            
         </tr>
         <?php }} ?>
      </table>
          
            
            </div>
          </div>
        </div>
      </div>
    </div>
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  </div>
  <p>&nbsp;</p>
            <center>Desenvolvido por 
              <a href="http://www.globalnetsis.com.br" title="acesse o website" target="_blank">Globalnetsis</a>
              - Todos os direitos reservados.
              </center>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-55649-19");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
